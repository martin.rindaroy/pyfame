from ctypes import c_int64, c_float, c_double, c_int, Structure

c_fame_index = c_int64
c_fame_date = c_int64
c_fame_numeric = c_float
c_fame_precision = c_double
c_fame_boolean = c_int
c_fame_type = c_int
c_fame_freq = c_fame_type


class c_fame_range(Structure):
    _fields_ = [('r_freq', c_fame_freq),
                ('r_start', c_fame_index),
                ('r_end', c_fame_index)]

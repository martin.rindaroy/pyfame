import pandas as pd

mode = pd.DataFrame({'code': ['HRMODE', 'HCMODE', 'HOMODE', 'HUMODE', 'HSMODE', 'HWMODE', 'HDMODE'],
                     'value': [1, 2, 3, 4, 5, 6, 7],
                     'description': ['READ', 'CREATE', 'OVERWRITE', 'UPDATE', 'SHARED', 'WRITE',
                                     'DIRECT READ and WRITE']})

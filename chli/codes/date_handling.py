import pandas as pd

fiscal_year = pd.DataFrame({'code': ['HFYFST', 'HFYLST', 'HFYAUT'],
                            'value': [1, 2, 3],
                            'description': ['FIRST', 'LAST', 'AUTO']})

month = pd.DataFrame(
    {'code': ['HJAN', 'HFEB', 'HMAR', 'HAPR', 'HMAY', 'HJUN', 'HJUL', 'HAUG', 'HSEP', 'HOCT', 'HNOV', 'HDEC'],
     'value': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
     'description': ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER',
                     'NOVEMBER', 'DECEMBER']})

biweekday = pd.DataFrame({'code': ['HASUN', 'HAMON', 'HATUE', 'HAWED', 'HATHU', 'HAFRI', 'HASAT', 'HBSUN', 'HBMON',
                                   'HBTUE', 'HBWED', 'HBTHU', 'HBFRI', 'HBSAT'],
                          'value': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14],
                          'description': ['ASUNDAY', 'AMONDAY', 'ATUESDAY', 'AWEDNESDAY', 'ATHURSDAY', 'AFRIDAY',
                                          'ASATURDAY', 'BSUNDAY', 'BMONDAY', 'BTUESDAY', 'BWEDNESDAY', 'BTHURSDAY',
                                          'BFRIDAY', 'BSATURDAY']})

weekday = pd.DataFrame({'code': ['HSUN', 'HMON', 'HTUE', 'HWED', 'HTHU', 'HFRI', 'HSAT'],
                        'value': [1, 2, 3, 4, 5, 6, 7],
                        'description': ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY']})

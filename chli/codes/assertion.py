import pandas as pd

types = pd.DataFrame({'code': ['HSTRNG', 'HUNCHG'],
                      'value': [4, 0],
                      'description': ['Assertion contained in assert argument',
                                      'Used to test for any changes to databases']})

perspectives = pd.DataFrame({'code': ['HSERVR', 'HCLNT', 'HCHANL'],
                             'value': [0, 1, 2],
                             'description': ["Determine changes from server's perspective",
                                             "Determine changes from client's perspective",
                                             "Determine changes based on database keys"]})

groupings = pd.DataFrame({'code': ['HREAD', 'HWRITE', 'HALL'],
                          'value': [0, 1, 2],
                          'description': ["Examine databases open for READ access",
                                          "Examine databases open for WRITE access", "Examine all open databases"]})

import pandas as pd

selection = pd.DataFrame({'code': ['HBEGIN', 'HEND', 'HINTVL'],
                          'value': [1, 2, 3],
                          'description': ['Beginning of period', 'End of period', 'Period interval']})

relation = pd.DataFrame({'code': ['HBEFOR', 'HAFTER', 'HCONT'],
                         'value': [1, 2, 3],
                         'description': ['Before', 'After', 'Contains']})

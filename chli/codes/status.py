import pandas as pd

codes = pd.DataFrame({'code': ['HSUCC', 'HINITD', 'HNINIT', 'HFIN', 'HBFILE', 'HBMODE', 'HBKEY', 'HBSRNG', 'HBERNG',
                               'HBNRNG', 'HNOOBJ', 'HBRNG', 'HDUTAR', 'HBOBJT', 'HBFREQ', 'HTRUNC', 'HNPOST', 'HFUSE',
                               'HNFMDB', 'HRNEXI', 'HCEXI', 'HNRESW', 'HBCLAS', 'HBOBSV', 'HBBASI', 'HOEXI', 'HBMONT',
                               'HBFLAB', 'HBMISS', 'HBINDX', 'HNWILD', 'HBNCHR', 'HBGROW', 'HQUOTA', 'HOLDDB', 'HMPOST',
                               'HSPCDB', 'HBFLAG', 'HPACK', 'HNEMPT', 'HBATTR', 'HDUP', 'HBYEAR', 'HBPER', 'HBDAY',
                               'HBDATE', 'HBSEL', 'HBREL', 'HBTIME', 'HBCPU', 'HEXPIR', 'HBPROD', 'HBUNIT', 'HBCNTX',
                               'HLOCKD', 'HNETCN', 'HNFAME', 'HNBACK', 'HSUSPN', 'HBSRVR', 'HCLNLM', 'HBUSER', 'HSRVST',
                               'HBOPT', 'HBOPTV', 'HNSUPP', 'HBLEN', 'HNULLP', 'HNWFEA', 'HBGLNM', 'HCLCHN', 'HDPRMC',
                               'HWKOPN', 'HNOMEM', 'HBFUNC', 'HUNEXP', 'HBVER', 'HNFILE', 'HMFILE', 'HSCLLM', 'HDBCLM',
                               'HSNFIL', 'HSMFIL', 'HRESFD', 'HTMOUT', 'HCHGAC', 'HFMENV', 'HLICFL', 'HLICNS', 'HRMTDB',
                               'HBCONN', 'HABORT', 'HNCONN', 'HNMCA', 'HBATYP', 'HBASRT', 'HBPRSP', 'HBGRP', 'HNLOCL',
                               'HDHOST', 'HOPENW', 'HOPEND', 'HNTWIC', 'HPWWOU', 'HMAXDB', 'HREMSUP', 'HBADVAL',
                               'HNOMAP', 'HROSCONN', 'HRKEYINV', 'HRUSERINV', 'HROSTRANS', 'HRDISTRANS', 'HRHANDINV',
                               'HNSUPDB', 'HCVTDB', 'HOBSFUNC', 'HBV3TYPE', 'HBV3RANGE', 'HBV3DATE', 'HNTOOLONG',
                               'HUTOOLONG', 'HFUTURE_TYPE', 'HBTYPE', 'HOBJSIZE', 'HUSRPWTOOLONG', 'HNFSRCLONG',
                               'HFRMSYNTAXERR', 'HNOFORMV3', 'HIFAIL', 'HFAMER'],
                      'value': [0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 13, 14, 15, 16, 17, 18, 20, 21, 22, 23, 24, 25, 26, 27,
                                28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
                                50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71,
                                73, 74, 75, 76, 77, 79, 80, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100,
                                101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 117, 119, 120,
                                121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137,
                                138, 139, 145, 146, 147, 511, 513],
                      'description': ["Success.", "The HLI has already been initialized.",
                                      "The HLI has not been initialized.",
                                      "The HLI has already been finished and cannot be reinitialized in the same "
                                      "session.",
                                      "A bad file name was given.",
                                      "A bad or unauthorized database file access mode was given, or the given "
                                      "database file is not open for the requested access.",
                                      "A bad database key was given.",
                                      "A bad starting year or period was given for a range.",
                                      "A bad ending year or period was given for a range.",
                                      "A bad number of observations was given for a range.",
                                      "The given object does not exist.", "A bad range was given.",
                                      "The target object already exists.",
                                      "A bad object type was given or the given object has the wrong type.",
                                      "A bad frequency was given or the given object has the wrong frequency.",
                                      "The oldest data has been truncated.",
                                      "The database has not been posted or closed.",
                                      "The database file is already in use.", "The file is not a FAME database.",
                                      "Trying to read or update a database file that does not exist.",
                                      "Trying to create a database file that already exists.",
                                      "The name given is not a legal FAME name or is a FAME reserved word.",
                                      "A bad object class was given or the given object has the wrong class.",
                                      "A bad OBSERVED attribute was given.", "A bad BASIS attribute was given.",
                                      "The data object already exists.", "A bad month was given.",
                                      "A bad fiscal year label was given.", "A bad missing value type was given.",
                                      "A bad value index was given.",
                                      "Wildcarding has not been initialized for the database or has since been "
                                      "invalidated.",
                                      "A bad number of characters was given.", "A bad growth factor was given.",
                                      "Maximum number of files already open or no disk space is available.",
                                      "Cannot update or share old databases.", "The database must be posted.",
                                      "Cannot write to a special database.", "A bad flag was given.",
                                      "Cannot perform operation on packed database.", "Database is not empty.",
                                      "A bad attribute name was given.", "A duplicate was ignored.",
                                      "A bad year was given.", "A bad period was given.", "A bad day was given.",
                                      "A bad date was given.", "A bad date selector was given.",
                                      "A bad date relation was given.", "A bad hour, minute, or second was given.",
                                      "Unauthorized CPU ID or hardware type.", "Expired dead date.",
                                      "Unauthorized product.", "A bad number of units was given.",
                                      "This operation not allowed in the current context.", "The object is locked.",
                                      "Could not connect to another machine to open the database. Possible reasons "
                                      "include: bad user or password, server not running on other machine. Also used "
                                      "for lost connection.",
                                      "FAME process has been terminated.", "Connection to server terminated.",
                                      "Access to a remote database has been temporarily suspended. Try again later.",
                                      "The requested FRDB protocol is not supported by the server.",
                                      "Database server hard client limit exceeded.",
                                      "Bad user name or password in file spec for remote host.",
                                      "Could not start server process on remote host.", "Bad option.",
                                      "Bad value for this option. For string options, there were more than 100 words "
                                      "in the option's value.",
                                      "Operation not supported on this database.", "A bad length was given.",
                                      "A NULL pointer was passed as an argument.",
                                      "Database contains new features unknown to this older HLI release. Link with a "
                                      "newer version of the HLI.",
                                      "An invalid name was specified for a GLNAME or GLFORMULA. Check the %k prefix "
                                      "of the name and the dimension of the database. For %1 names, the rest of the "
                                      "name must also be a valid name. Also used for invalid aliases of a GLNAME or "
                                      "GLFORMULA.",
                                      "A fatal I/O error or termination of server has caused the channel to be closed.",
                                      "Call to cfmopre or cfmoprc by a client that already has a dbkey for a KIND "
                                      "REMOTE channel to the MCADBS server.",
                                      "The cfmopwk function was called when a work database is already open.",
                                      "Not enough memory for requested operation.",
                                      "Attempt to use obsolete function on an FRDB database undergoing asynchronous "
                                      "changes.",
                                      "Unexpected error condition, check system error number.",
                                      "The license file does not support this version.", "System file table is full.",
                                      "Too many files open (for this process).",
                                      "Database server soft client limit exceeded.",
                                      "Database server database client limit exceeded.",
                                      "Database server system file table full.",
                                      "Database server process has too many files open.",
                                      "Database server could not open reserved file descriptor (for some unexpected "
                                      "reason).",
                                      "Database server didn't respond within the time limit.",
                                      "You have a database open for read access and now want to open the same database "
                                      "for write access. While you now have permission write access, you did not have "
                                      "it at the time of the previous open. In this situation you must first close the "
                                      "channel on which you had read access before you can get write access.",
                                      "The FAME environment variable must be set to the location of FAME licensing "
                                      "files before starting an HLI application.",
                                      "A FAME licensing file was not found in the directory specified by the FAME "
                                      "environment variable.",
                                      "Unable to acquire a license to run the HLI.",
                                      "This function is not valid for a remote database open for WRITE access.",
                                      "A bad connection key was given.", "Pending unit of work aborted.",
                                      "Specified database key is not open on a connection.",
                                      "Remote server channel or usage of assertion expression requires connection to"
                                      " an MCADBS server.",
                                      "A bad assertion type was specified.", "A bad assertion was specified.",
                                      "A bad perspective was specified.", "A bad grouping was specified.",
                                      "A local open of a FRDB writeable database is not allowed.",
                                      "The write server for the database is not running on this host.",
                                      "Database already open for WRITE access; DIRECT access not allowed.",
                                      "Database already open for DIRECT access; no other DIRECT or WRITE access "
                                      "allowed.",
                                      "The server's database is already open in the specified mode and cannot be "
                                      "opened twice.",
                                      "Password specified without username.",
                                      "The database has reached its maximum size and the current operation has "
                                      "failed.  All changes since the last close or post have been lost.",
                                      "Service does not support KIND REMOTE.", "A bad data value was found.",
                                      "Database opened with CLASSIC access; memory mapping did not succeed.",
                                      "Secure connection is not supported by this server.",
                                      "The provided KEY is invalid.", "The provided USER name is invalid.",
                                      "Only secure transport is allowed.", "Encryption is not supported.",
                                      "The handshake was invalid.", "Unsupported database version or format.",
                                      "Database is version 2 (standard) and must be converted to version 3 or "
                                      "version 4 by compress.",
                                      "Date or period number out of range for int argument, new frequency not "
                                      "supported, or returned name exceeds 64 characters.",
                                      "The type or frequency of the object or attribute is not supported in "
                                      "version 3 databases.",
                                      "The range of the series exceeds the limit supported in version 3 databases.",
                                      "A date value of the object or attribute is out of bounds for version 3 "
                                      "databases.",
                                      "The name is too long.", "The name is too long for a special UDA namelist.",
                                      "A object of Future Type was accessed.", "Bad Type",
                                      "The current (or previous) object is too large.",
                                      "Username or password too long in cfmopcn.", "Source string is too long",
                                      "Parsing error in the source for the formula",
                                      "In case of V3 databases, if TURN FORMULA PROTECTION is ON",
                                      "HLI internal failure.",
                                      "Error from a FAME-like server. Call cfmferr for the text of the message. "
                                      "HFAMER is > 512 for compatibility with cfmfame in earlier releases."]})

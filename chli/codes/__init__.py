import pandas as pd

import chli.codes.assertion as assertion
import chli.codes.boolean as boolean
import chli.codes.data_object as data_object
import chli.codes.data_value as data_value
import chli.codes.database_access as database_access
import chli.codes.date_handling as date_handling
import chli.codes.date_selection_and_relation as date_selection_and_relation
import chli.codes.frequency as frequency
import chli.codes.missing_value as missing_value
import chli.codes.special_namelist_index as special_namelist_index
import chli.codes.status as status


def from_code(codes: pd.DataFrame, code: str):
    return codes.set_index('code').loc[code]


def from_value(codes: pd.DataFrame, value: str):
    return codes.set_index('value').loc[value]

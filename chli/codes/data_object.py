import pandas as pd

basis = pd.DataFrame({'type': ['old_basis_code', 'old_basis_code', 'old_basis_code', 'frequency_code', 'frequency_code',
                               'frequency_code'],
                      'code': ['HBSUND', 'HBSDAY', 'HBSBUS', 'HUNDFX', 'HDAILY', 'HBUSNS'],
                      'value': [0, 1, 2, 0, 8, 9],
                      'description': ['Undefined', 'DAILY', 'BUSINESS', 'Undefined', 'DAILY', 'BUSINESS']})

observed = pd.DataFrame({'code': ['HOBUND', 'HOBBEG', 'HOBEND', 'HOBAVG', 'HOBSUM', 'HOBANN', 'HOBFRM', 'HOBHI',
                                  'HOBLO'],
                         'value': [0, 1, 2, 3, 4, 5, 6, 7, 8],
                         'description': ['Undefined', 'BEGINNING', 'ENDING', 'AVERAGED', 'SUMMED', 'ANNUALIZED',
                                         'FORMULA', 'HIGH', 'LOW']})

class_ = pd.DataFrame({'code': ['HSERIE', 'HSCALA', 'HFRMLA', 'HGLNAM', 'HGLFOR'],
                       'value': [1, 2, 3, 5, 6],
                       'description': ['SERIES', 'SCALAR', 'FORMULA', 'GLNAME', 'GLFORMULA']})

type_ = pd.DataFrame({'code': ['HUNDFT', 'HNUMRC', 'HNAMEL', 'HBOOLN', 'HSTRNG', 'HPRECN', 'HDATE'],
                      'value': [0, 1, 2, 3, 4, 5, 6],
                      'description': ['Undefined', 'NUMERIC', 'NAMELIST', 'BOOLEAN', 'STRING', 'PRECISION',
                                      'General DATE']})

import pandas as pd

type_ = pd.DataFrame({'code': ['HNMVAL', 'HNCVAL', 'HNAVAL', 'HNDVAL', 'HMGVAL'],
                      'value': [0, 1, 2, 3, 4],
                      'description': ["Normal value; not missing or magic", "Missing NC - Not Computable",
                                      "Missing NA - Not Available", "Missing ND - Not Defined",
                                      "Magic value; for internal use only"]})

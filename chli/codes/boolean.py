import pandas as pd

flags = pd.DataFrame({'code': ['HNO', 'HYES'],
                      'value': [0, 1],
                      'description': ['NO or FALSE', 'YES or TRUE']})

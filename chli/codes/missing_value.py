import pandas as pd


def _x(a, b):
    return pd.DataFrame({'code': [f'F{a}NC', f'F{a}ND', f'F{a}NA'],
                         'description': [f'{b} missing NC - Not Computable', f'{b} missing ND - Not Defined',
                                         f'{b} missing NA - Not Available']})


translation_flags = pd.DataFrame({'code': ['HNTMIS', 'HTMIS'],
                                  'value': [0, 1],
                                  'description': ['Do not translate missing values', 'Translate missing values']})

numeric = _x('NUM', 'Numeric')
precision = _x('PRC', 'Precision')
boolean = _x('BOO', 'Boolean')
date = _x('DAT', 'Date')
string = pd.concat([pd.DataFrame({'code': ['HSMLEN'],
                                  'description': ['Length of a string missing value']}),
                    _x('STR', 'String')],
                   ignore_index=True)

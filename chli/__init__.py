import pathlib
from ctypes import WinDLL
from typing import Optional

import chli.codes as codes
import chli.constants as constants
import chli.types as types
from chli.functions import (cfmini, cfmfin, cfmver, cfmlerr, cfmferr, cfmopcn, cfmgcid, cfmcmmt, cfmabrt, cfmclcn,
                            cfmopdb, cfmopdc, cfmoprc, cfmopre, cfmopwk, cfmpodb, cfmcldb, cfmsopt, cfmnwob, cfmdlob,
                            cfmsali, cfmrmev, cfmddoc, cfmsdoc, fame_info, fame_init_range_from_indexes,
                            fame_init_range_from_end_numobs, fame_init_range_from_start_numobs, fame_time_to_index,
                            fame_index_to_time, fame_get_range_numobs, fame_get_booleans, fame_get_dates,
                            fame_get_numerics, fame_get_precisions, fame_get_strings, fame_write_booleans,
                            fame_write_dates, fame_write_numerics, fame_write_precisions, fame_write_strings, cfmgdba,
                            cfmddes, cfmsdes, fame_init_wildcard, fame_get_next_wildcard, fame_free_wildcard, cfmfame)

CHLI_DLL_PATH: str = str(pathlib.Path(__file__ + '/../chli.dll').absolute())
LIB: Optional[WinDLL] = None

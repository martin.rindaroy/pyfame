# --- FAME NAME SIZE --- #

HNAMLEN_V3 = 64
HNAMLEN_V4 = 242
HNAMLEN = HNAMLEN_V4
HNAMSIZ = (HNAMLEN + 1)
# ---------------------- #

# --- Numeric --- #
FNUMNA = None
FNUMNC = None
FNUMND = None
# --------------- #

# --- Precision --- #
FPRCNA = None
FPRCNC = None
FPRCND = None
# ----------------- #

# --- Boolean --- #
FBOONA = None
FBOONC = None
FBOOND = None
# --------------- #

# --- Date --- #
FDATNA = None
FDATNC = None
FDATND = None
FAME_INDEX_NA = None
FAME_INDEX_NC = None
FAME_INDEX_ND = None
# ------------ #

# --- String --- #
HSMLEN = 2  # Length of a string missing value
FSTRNA = None
FSTRNC = None
FSTRND = None
# -------------- #

# --- Flags --- #
HNO = 0
HYES = 1
# ------------- #

# --- Special index --- #
HNLALL = -1  # indicates the whole name list
# --------------------- #

# --- Maximum string scalar length --- #
HLI_MAX_STR_LEN = 65534
# ------------------------------------ #

# --- The maximum length of a command sent from FAME to an HLI server via cfmsinp --- #
HMAXSCMD = 5002
# ----------------------------------------------------------------------------------- #

# --- The maximum length of a command sent from FAME to an HLI server via fame_get_server_input --- #
HLI_MAX_FAME_INPUT = HLI_MAX_STR_LEN
# ------------------------------------------------------------------------------------------------- #

# --- The maximum buffer sized needed for fame_get_server_input --- #
HLI_MAX_FAME_INPUT_SIZE = (HLI_MAX_STR_LEN + 1)
# ----------------------------------------------------------------- #

# --- The maximum length of a command sent from an HLI server to FAME (cfmfame) --- #
HLI_MAX_FAME_CMD = 1048578
# --------------------------------------------------------------------------------- #

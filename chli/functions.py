from ctypes import pointer, c_int, c_float, create_string_buffer, c_char_p, byref, cast, c_char, WinDLL, windll
from typing import Tuple

import numpy as np

import chli
from chli.constants import HLI_MAX_STR_LEN
from chli.types import (c_fame_range, c_fame_boolean, c_fame_date, c_fame_numeric, c_fame_precision, c_fame_freq,
                        c_fame_type, c_fame_index)


def cfmini() -> int:
    """
    Initialize the C HLI
    """
    chli.LIB = WinDLL(chli.CHLI_DLL_PATH)

    c_status = pointer(c_int())

    chli.LIB.cfmini(c_status)

    chli.constants.FBOONA = c_fame_boolean.in_dll(chli.LIB, 'FBOONA')
    chli.constants.FBOONC = c_fame_boolean.in_dll(chli.LIB, 'FBOONC')
    chli.constants.FBOOND = c_fame_boolean.in_dll(chli.LIB, 'FBOOND')

    chli.constants.FNUMNA = c_fame_numeric.in_dll(chli.LIB, 'FNUMNA')
    chli.constants.FNUMNC = c_fame_numeric.in_dll(chli.LIB, 'FNUMNC')
    chli.constants.FNUMND = c_fame_numeric.in_dll(chli.LIB, 'FNUMND')

    chli.constants.FPRCNA = c_fame_precision.in_dll(chli.LIB, 'FPRCNA')
    chli.constants.FPRCNC = c_fame_precision.in_dll(chli.LIB, 'FPRCNC')
    chli.constants.FPRCND = c_fame_precision.in_dll(chli.LIB, 'FPRCND')

    chli.constants.FSTRNA = (c_char * chli.constants.HSMLEN).in_dll(chli.LIB, 'FSTRNA')
    chli.constants.FSTRND = (c_char * chli.constants.HSMLEN).in_dll(chli.LIB, 'FSTRND')
    chli.constants.FSTRNC = (c_char * chli.constants.HSMLEN).in_dll(chli.LIB, 'FSTRNC')

    chli.constants.FDATNA = c_fame_date.in_dll(chli.LIB, 'FDATNA')
    chli.constants.FDATNC = c_fame_date.in_dll(chli.LIB, 'FDATNC')
    chli.constants.FDATND = c_fame_date.in_dll(chli.LIB, 'FDATND')

    chli.constants.FAME_INDEX_NA = c_fame_index.in_dll(chli.LIB, 'FAME_INDEX_NA')
    chli.constants.FAME_INDEX_NC = c_fame_index.in_dll(chli.LIB, 'FAME_INDEX_NC')
    chli.constants.FAME_INDEX_ND = c_fame_index.in_dll(chli.LIB, 'FAME_INDEX_ND')

    return c_status.contents.value


def cfmfin() -> int:
    """
    Finish the C HLI
    """
    c_status = pointer(c_int())

    chli.LIB.cfmfin(c_status)

    windll.kernel32.FreeLibrary(chli.LIB._handle)

    return c_status.contents.value


def cfmver() -> Tuple[int, float]:
    """
    Get the C HLI version number
    """
    c_status = pointer(c_int())
    c_ver = pointer(c_float())

    chli.LIB.cfmver(c_status, c_ver)

    return c_status.contents.value, c_ver.contents.value


def cfmlerr() -> Tuple[int, int]:
    """
    Get the length of the FAME error
    """
    c_status = pointer(c_int())
    c_len = pointer(c_int())

    chli.LIB.cfmlerr(c_status, c_len)

    return c_status.contents.value, c_len.contents.value


def cfmferr() -> Tuple[int, str]:
    """
    Get the text of the FAME error
    """
    status, length = cfmlerr()

    if status == 0:
        c_status = pointer(c_int())
        c_errtxt = create_string_buffer(b' ' * length)

        chli.LIB.cfmferr(c_status, c_errtxt)

        return c_status.contents.value, c_errtxt.value.decode().strip()
    else:
        return status, ''


def cfmopcn(service: str, hostname: str, username: str, password: str) -> Tuple[int, int]:
    """
    Open a connection to a FAME Database Server
    """
    c_status = pointer(c_int())
    c_connkey = pointer(c_int())
    c_service = c_char_p(service.encode('UTF-8'))
    c_hostname = c_char_p(hostname.encode('UTF-8'))
    c_username = c_char_p(username.encode('UTF-8'))
    c_password = c_char_p(password.encode('UTF-8'))

    chli.LIB.cfmopcn(c_status, c_connkey, c_service, c_hostname, c_username, c_password)

    return c_status.contents.value, c_connkey.contents.value


def cfmgcid(dbkey: int) -> Tuple[int, int]:
    """
    Get the connection key for a database key
    """
    c_status = pointer(c_int())
    c_dbkey = c_int(dbkey)
    c_connkey = pointer(c_int())

    chli.LIB.cfmgcid(c_status, c_dbkey, c_connkey)

    return c_status.contents.value, c_connkey.contents.value


def cfmcmmt(connkey: int) -> int:
    """
    Commit a unit of work on a connection
    """
    c_status = pointer(c_int())
    c_connkey = c_int(connkey)

    chli.LIB.cfmcmmt(c_status, c_connkey)

    return c_status.contents.value


def cfmabrt(connkey: int) -> int:
    """
    Abort a unit of work on a connection
    """
    c_status = pointer(c_int())
    c_connkey = c_int(connkey)

    chli.LIB.cfmabrt(c_status, c_connkey)

    return c_status.contents.value


def cfmclcn(connkey: int) -> int:
    """
    Close a connection to a FAME Database Server
    """
    c_status = pointer(c_int())
    c_connkey = c_int(connkey)

    chli.LIB.cfmclcn(c_status, c_connkey)

    return c_status.contents.value


def cfmopdb(dbname: str, mode: int) -> Tuple[int, int, str]:
    """
    Open a database
    """
    c_status = pointer(c_int())
    c_dbkey = pointer(c_int())
    c_dbname = create_string_buffer(dbname.encode('UTF-8'), len(dbname))
    c_mode = c_int(mode)

    chli.LIB.cfmopdb(c_status, c_dbkey, c_dbname, c_mode)

    return c_status.contents.value, c_dbkey.contents.value, c_dbname.value.decode()


def cfmopdc(dbname: str, mode: int, connkey: int) -> Tuple[int, int]:
    """
    Open a database on a connection
    """
    c_status = pointer(c_int())
    c_dbkey = pointer(c_int())
    c_dbname = create_string_buffer(dbname.encode('UTF-8'), len(dbname))
    c_mode = c_int(mode)
    c_connkey = c_int(connkey)

    chli.LIB.cfmopdc(c_status, c_dbkey, c_dbname, c_mode, c_connkey)

    return c_status.contents.value, c_dbkey.contents.value


def cfmoprc(connkey: int) -> Tuple[int, int]:
    """
    Open an analytical channel on a connection
    """
    c_status = pointer(c_int())
    c_dbkey = pointer(c_int())
    c_connkey = c_int(connkey)

    chli.LIB.cfmoprc(c_status, c_dbkey, c_connkey)

    return c_status.contents.value, c_dbkey.contents.value


def cfmopre(svname: str) -> Tuple[int, int, str]:
    """
    Open an analytical channel to a server
    """
    c_status = pointer(c_int())
    c_dbkey = pointer(c_int())
    c_svname = create_string_buffer(svname.encode('UTF-8'), len(svname))

    chli.LIB.cfmopre(c_status, c_dbkey, c_svname)

    return c_status.contents.value, c_dbkey.contents.value, c_svname.value.decode()


def cfmopwk() -> Tuple[int, int]:
    """
    Opens the WORK database of a FAME process. If the current C HLI process was started by an OPEN <KIND SERVER>
    command in a FAME session, cfmopwk gives access to the WORK database of the parent FAME process. If the C HLI
    process was not started by an OPEN <KIND SERVER> command, and if neither cfmfame nor cfmopwk has been called,
    cfmopwk starts a child FAME process and provides access to its WORK database. Otherwise, cfmopwk provides access
    to the WORK database of the existing child FAME process. In all cases, the WORK database is effectively opened
    with access update.
    """
    c_status = pointer(c_int())
    c_dbkey = pointer(c_int())

    chli.LIB.cfmopwk(c_status, c_dbkey)

    return c_status.contents.value, c_dbkey.contents.value


def cfmpodb(dbkey: int) -> int:
    """
    Post a database
    """
    c_status = pointer(c_int())
    c_dbkey = c_int(dbkey)

    chli.LIB.cfmpodb(c_status, c_dbkey)

    return c_status.contents.value


def cfmcldb(dbkey: int) -> int:
    """
    Close a database or analytical channel
    """
    c_status = pointer(c_int())
    c_dbkey = c_int(dbkey)

    chli.LIB.cfmcldb(c_status, c_dbkey)

    return c_status.contents.value


def cfmsopt(optnam: str, optval: str) -> Tuple[int, str, str]:
    """
    Setting options in the C HLI
    """
    c_status = pointer(c_int())
    c_optnam = create_string_buffer(optnam.encode('UTF-8'), len(optnam))
    c_optval = create_string_buffer(optval.encode('UTF-8'), len(optval))

    chli.LIB.cfmsopt(c_status, c_optnam, c_optval)

    return c_status.contents.value, c_optnam.value.decode(), c_optval.value.decode()


def cfmnwob(dbkey: int, objnam: str, class_: int, freq: int, type_: int, basis: int, observ: int) -> Tuple[int, str]:
    """
    Add a new object to a database
    """
    c_status = pointer(c_int())
    c_dbkey = c_int(dbkey)
    c_objnam = create_string_buffer(objnam.encode('UTF-8'), len(objnam))
    c_class = c_int(class_)
    c_freq = c_int(freq)
    c_type = c_int(type_)
    c_basis = c_int(basis)
    c_observ = c_int(observ)

    chli.LIB.cfmnwob(c_status, c_dbkey, c_objnam, c_class, c_freq, c_type, c_basis, c_observ)

    return c_status.contents.value, c_objnam.value.decode()


def cfmdlob(dbkey: int, objnam: str) -> Tuple[int, str]:
    """
    Delete an object from a database
    """
    c_status = pointer(c_int())
    c_dbkey = c_int(dbkey)
    c_objnam = create_string_buffer(objnam.encode('UTF-8'), len(objnam))

    chli.LIB.cfmdlob(c_status, c_dbkey, c_objnam)

    return c_status.contents.value, c_objnam.value.decode()


def cfmsali(dbkey: int, objnam: str, aliass: str) -> Tuple[int, str]:
    """
    Sets the aliases for a given object
    """
    c_status = pointer(c_int())
    c_dbkey = c_int(dbkey)
    c_objnam = create_string_buffer(objnam.encode('UTF-8'), len(objnam))
    c_aliass = c_char_p(aliass.encode('UTF-8'))

    chli.LIB.cfmsali(c_status, c_dbkey, c_objnam, c_aliass)

    return c_status.contents.value, c_objnam.value.decode()


def cfmrmev(dbkey: int, expr: str, optns: str, wdbkey: int, objnam: str) -> int:
    """
    Directs the remote server to evaluate a given expression and store the result in a FAME database
    """
    c_status = pointer(c_int())
    c_dbkey = c_int(dbkey)
    c_expr = c_char_p(expr.encode('UTF-8'))
    c_optns = c_char_p(optns.encode('UTF-8'))
    c_wdbkey = c_int(wdbkey)
    c_objnam = c_char_p(objnam.encode('UTF-8'))

    chli.LIB.cfmrmev(c_status, c_dbkey, c_expr, c_optns, c_wdbkey, c_objnam)

    return c_status.contents.value


def cfmddoc(dbkey: int, doc: str) -> int:
    """
    Sets the documentation of a FAME database
    """
    c_status = pointer(c_int())
    c_dbkey = c_int(dbkey)
    c_doc = c_char_p(doc.encode('UTF-8'))

    chli.LIB.cfmddoc(c_status, c_dbkey, c_doc)

    return c_status.contents.value


def cfmddes(dbkey: int, desc: str) -> int:
    """
    Sets the description of a FAME database
    """
    c_status = pointer(c_int())
    c_dbkey = c_int(dbkey)
    c_desc = c_char_p(desc.encode('UTF-8'))

    chli.LIB.cfmddes(c_status, c_dbkey, c_desc)

    return c_status.contents.value


def cfmsdoc(dbkey: int, objnam: str, doc: str) -> Tuple[int, str]:
    """
    Sets the documentation of an object in a FAME database
    """
    c_status = pointer(c_int())
    c_dbkey = c_int(dbkey)
    c_objnam = create_string_buffer(objnam.encode('UTF-8'), len(objnam))
    c_doc = c_char_p(doc.encode('UTF-8'))

    chli.LIB.cfmsdoc(c_status, c_dbkey, c_objnam, c_doc)

    return c_status.contents.value, c_objnam.value.decode()


def cfmsdes(dbkey: int, objnam: str, desc: str) -> Tuple[int, str]:
    """
    Sets the description of an object in a FAME database
    """
    c_status = pointer(c_int())
    c_dbkey = c_int(dbkey)
    c_objnam = create_string_buffer(objnam.encode('UTF-8'), len(objnam))
    c_desc = c_char_p(desc.encode('UTF-8'))

    chli.LIB.cfmsdes(c_status, c_dbkey, c_objnam, c_desc)

    return c_status.contents.value, c_objnam.value.decode()


def cfmgdba(dbkey: int) -> Tuple[int, int, int, int, int, int, int, str, str]:
    """
     Returns the attributes for a given FAME database.
    """
    c_status = pointer(c_int())
    c_dbkey = c_int(dbkey)
    c_cyear = pointer(c_int())
    c_cmonth = pointer(c_int())
    c_cday = pointer(c_int())
    c_myear = pointer(c_int())
    c_mmonth = pointer(c_int())
    c_mday = pointer(c_int())
    c_desc = create_string_buffer(b' ' * HLI_MAX_STR_LEN)
    c_doc = create_string_buffer(b' ' * HLI_MAX_STR_LEN)

    chli.LIB.cfmgdba(c_status, c_dbkey, c_cyear, c_cmonth, c_cday, c_myear, c_mmonth, c_mday, c_desc, c_doc)

    return (c_status.contents.value,
            c_cyear.contents.value,
            c_cmonth.contents.value,
            c_cday.contents.value,
            c_myear.contents.value,
            c_mmonth.contents.value,
            c_mday.contents.value,
            c_desc.value.decode().strip(),
            c_doc.value.decode().strip())


def cfmfame(cmd: str) -> int:
    """
    Sends a command to be executed in a FAME process
    """
    c_status = pointer(c_int())
    c_cmd = c_char_p(cmd.encode('UTF-8'))

    chli.LIB.cfmfame(c_status, c_cmd)

    return c_status.contents.value


def fame_info(dbkey: int, oname: str) -> Tuple[int, int, int, int, int, int, int, int, int, int, str, int, str, int]:
    """
    Provides information about an object in a FAME database, similar to the WHATS command in FAME 4GL
    """
    c_dbkey = c_int(dbkey)
    c_oname = c_char_p(oname.encode('UTF-8'))
    c_oclass = pointer(c_int())
    c_type = pointer(c_fame_type())
    c_freq = pointer(c_fame_freq())
    c_findex = pointer(c_fame_index())
    c_lindex = pointer(c_fame_index())
    c_basis = pointer(c_fame_freq())
    c_observ = pointer(c_int())
    c_cdate = pointer(c_fame_index())
    c_mdate = pointer(c_fame_index())
    c_desc = create_string_buffer(b' ' * HLI_MAX_STR_LEN)
    c_indesclen = c_int(HLI_MAX_STR_LEN - 1)
    c_outdesclen = pointer(c_int())
    c_doc = create_string_buffer(b' ' * HLI_MAX_STR_LEN)
    c_indoclen = c_int(HLI_MAX_STR_LEN - 1)
    c_outdoclen = pointer(c_int())

    status = chli.LIB.fame_info(c_dbkey, c_oname, c_oclass, c_type, c_freq, c_findex, c_lindex, c_basis, c_observ,
                                c_cdate, c_mdate, c_desc, c_indesclen, c_outdesclen, c_doc, c_indoclen, c_outdoclen)

    return (status, c_oclass.contents.value, c_type.contents.value, c_freq.contents.value, c_findex.contents.value,
            c_lindex.contents.value, c_basis.contents.value, c_observ.contents.value, c_cdate.contents.value,
            c_mdate.contents.value, c_desc.value.decode().strip(), c_outdesclen.contents.value,
            c_doc.value.decode().strip(), c_outdoclen.contents.value)


def fame_init_wildcard(dbkey: int, wildname: str, wildonly: int, wildstart: str = '') -> Tuple[int, int]:
    """
    Initializes a FAME wildcard specification in the given database.
    Call fame_get_next_wildcard with wildkey to retrieve each name matching the wildcard specification.
    When the wildkey is no longer required, call fame_free_wildcard to free any resources associated with the wildkey.
    """
    c_dbkey = c_int(dbkey)
    c_wildkey = pointer(c_int())
    c_wildname = c_char_p(wildname.encode('UTF-8'))
    c_wildonly = c_int(wildonly)
    c_wildstart = c_char_p(wildstart.encode('UTF-8'))

    status = chli.LIB.fame_init_wildcard(c_dbkey, c_wildkey, c_wildname, c_wildonly, c_wildstart)

    return status, c_wildkey.contents.value


def fame_get_next_wildcard(wildkey: int) -> Tuple[int, str, int, int, int, int, int]:
    """
    provides information about the next object matching the wildcard represented by the specified wildkey. You must
    call the fame_init_wildcard function before calling the fame_get_next_wildcard function to obtain the wildkey.
    """
    c_wildkey = c_int(wildkey)
    c_objname = create_string_buffer(HLI_MAX_STR_LEN)
    c_class = pointer(c_int())
    c_type = pointer(c_fame_type())
    c_freq = pointer(c_fame_freq())
    c_start = pointer(c_fame_index())
    c_end = pointer(c_fame_index())
    c_inlen = c_int(HLI_MAX_STR_LEN - 1)
    c_outlen = pointer(c_int())

    status = chli.LIB.fame_get_next_wildcard(c_wildkey, c_objname, c_class, c_type, c_freq, c_start, c_end, c_inlen,
                                             c_outlen)

    return (status, c_objname.value.decode(), c_class.contents.value, c_type.contents.value, c_freq.contents.value,
            c_start.contents.value, c_end.contents.value)


def fame_free_wildcard(wildkey: int) -> int:
    """
    Frees internal C HLI resources associated with a wildcard instance. After calling fame_free_wildcard, additional
    calls to fame_get_next_wildcard for the same wildkey will return the HNOOBJ status code. When a set of set of
    wildcard matches has been completely iterated by calling fame_get_next_wildcard, a status of HNOOBJ is returned.
    The resources associated with the wildkey are automatically freed, and no call to fame_free_wildcard is explicitly
    necessary. The fame_free_wildcard function should explicitly be called for any wildkey which is no longer required
    and has not been fully iterated.
    """
    c_wildkey = c_int(wildkey)

    status = chli.LIB.fame_free_wildcard(c_wildkey)

    return status


def fame_init_range_from_indexes(freq: int, start: int, end: int) -> Tuple[int, c_fame_range]:
    """
    Sets up a c_fame_range type to use for later reading or writing of data.
    """
    c_range = pointer(c_fame_range())
    c_freq = c_int(freq)
    c_start = c_int(start)
    c_end = c_int(end)

    status = chli.LIB.fame_init_range_from_indexes(c_range, c_freq, c_start, c_end)

    return status, c_range.contents


def fame_init_range_from_end_numobs(freq: int, end: int, numobs: int) -> Tuple[int, c_fame_range]:
    """
    Sets up a c_fame_range type to use for later reading or writing of data.
    """
    c_range = c_fame_range()
    c_freq = c_int(freq)
    c_end = c_int(end)
    c_numobs = c_int(numobs)

    status = chli.LIB.fame_init_range_from_end_numobs(byref(c_range), c_freq, c_end, c_numobs)

    return status, c_range


def fame_init_range_from_start_numobs(freq: int, start: int, numobs: int) -> Tuple[int, c_fame_range]:
    """
    Sets up a c_fame_range type to use for later reading or writing of data.
    """
    c_range = c_fame_range()
    c_freq = c_int(freq)
    c_start = c_int(start)
    c_numobs = c_int(numobs)

    status = chli.LIB.fame_init_range_from_start_numobs(byref(c_range), c_freq, c_start, c_numobs)

    return status, c_range


def fame_time_to_index(freq: int, year: int, month: int, day: int, hour: int, minute: int, second: int,
                       millisecond: int, relate: int = 3) -> Tuple[int, int]:
    """
    Converts a year, month, day, hour, minute, second, and millisecond to a date at a specified frequency.
    """
    c_date = pointer(c_int())
    c_freq = c_int(freq)
    c_year = c_int(year)
    c_month = c_int(month)
    c_day = c_int(day)
    c_hour = c_int(hour)
    c_minute = c_int(minute)
    c_second = c_int(second)
    c_millisecond = c_int(millisecond)
    c_relate = c_int(relate)

    status = chli.LIB.fame_time_to_index(c_freq, c_date, c_year, c_month, c_day, c_hour, c_minute, c_second,
                                         c_millisecond, c_relate)

    return status, c_date.contents.value


def fame_index_to_time(freq: int, date: int) -> Tuple[int, int, int, int, int, int, int, int]:
    """
    Returns the time stamp of the beginning of a period of a calendar.
    """
    c_date = c_fame_date(date)
    c_freq = c_fame_freq(freq)
    c_year = pointer(c_int())
    c_month = pointer(c_int())
    c_day = pointer(c_int())
    c_hour = pointer(c_int())
    c_minute = pointer(c_int())
    c_second = pointer(c_int())
    c_millisecond = pointer(c_int())

    status = chli.LIB.fame_index_to_time(c_freq, c_date, c_year, c_month, c_day, c_hour, c_minute, c_second,
                                         c_millisecond)

    return (status, c_year.contents.value, c_month.contents.value, c_day.contents.value, c_hour.contents.value,
            c_minute.contents.value, c_second.contents.value, c_millisecond.contents.value)


def fame_get_range_numobs(range_: c_fame_range) -> Tuple[int, int]:
    """
    Provides the number of observations in a range.
    """
    c_range = pointer(range_)
    c_numobs = pointer(c_int())

    status = chli.LIB.fame_get_range_numobs(c_range, c_numobs)

    return status, c_numobs.contents.value


def _fame_get_values(c_func, dbkey: int, oname: str, range_: c_fame_range, c_na, c_nc, c_nd, na, nc, nd, c_type,
                     np_type: str):
    status, numobs = fame_get_range_numobs(range_)

    if status == 0:
        c_dbkey = c_int(dbkey)
        c_oname = c_char_p(oname.encode('UTF-8'))
        c_range = pointer(range_)
        c_valary = pointer((c_type * numobs)())

        status = c_func(c_dbkey, c_oname, c_range, c_valary)

        if status == 0:
            values = np.ndarray((len(c_valary.contents),), np_type, c_valary.contents, order='C').astype(object)

            values[np.argwhere(values == c_na)] = na
            values[np.argwhere(values == c_nc)] = nc
            values[np.argwhere(values == c_nd)] = nd

            return status, values
        else:
            return status, np.array([])

    else:
        return status, np.array([])


def fame_get_booleans(dbkey: int, oname: str, range_: c_fame_range) -> Tuple[int, np.ndarray]:
    """
    Gets a range of boolean observations from a boolean type series or scalar object.
    """
    return _fame_get_values(c_func=chli.LIB.fame_get_booleans,
                            dbkey=dbkey,
                            oname=oname,
                            range_=range_,
                            c_na=chli.constants.FBOONA,
                            c_nc=chli.constants.FBOONC,
                            c_nd=chli.constants.FBOOND,
                            na=np.nan,
                            nc=np.nan,
                            nd=np.nan,
                            c_type=c_fame_boolean,
                            np_type='i')


def fame_get_dates(dbkey: int, oname: str, range_: c_fame_range) -> Tuple[int, np.ndarray]:
    """
    Gets a range of date observations from a date type series or scalar object.
    """
    return _fame_get_values(c_func=chli.LIB.fame_get_dates,
                            dbkey=dbkey,
                            oname=oname,
                            range_=range_,
                            c_na=chli.constants.FDATNA,
                            c_nc=chli.constants.FDATNC,
                            c_nd=chli.constants.FDATND,
                            na=np.datetime64('NaT'),
                            nc=np.datetime64('NaT'),
                            nd=np.datetime64('NaT'),
                            c_type=c_fame_date,
                            np_type='q')


def fame_get_numerics(dbkey: int, oname: str, range_: c_fame_range) -> Tuple[int, np.ndarray]:
    """
    Gets a range of numeric observations from a numeric type series or scalar object.
    """
    return _fame_get_values(c_func=chli.LIB.fame_get_numerics,
                            dbkey=dbkey,
                            oname=oname,
                            range_=range_,
                            c_na=chli.constants.FNUMNA,
                            c_nc=chli.constants.FNUMNC,
                            c_nd=chli.constants.FNUMND,
                            na=np.nan,
                            nc=np.nan,
                            nd=np.nan,
                            c_type=c_fame_numeric,
                            np_type='f')


def fame_get_precisions(dbkey: int, oname: str, range_: c_fame_range) -> Tuple[int, np.ndarray]:
    """
    Gets a range of precision observations from a precision type series or scalar object.
    """
    return _fame_get_values(c_func=chli.LIB.fame_get_precisions,
                            dbkey=dbkey,
                            oname=oname,
                            range_=range_,
                            c_na=chli.constants.FPRCNA,
                            c_nc=chli.constants.FPRCNC,
                            c_nd=chli.constants.FPRCND,
                            na=np.nan,
                            nc=np.nan,
                            nd=np.nan,
                            c_type=c_fame_precision,
                            np_type='d')


def fame_get_strings(dbkey: int, oname: str, range_: c_fame_range) -> Tuple[int, np.ndarray]:
    """
    Gets a range of string observations from a string type series or scalar object.
    """
    status, numobs = fame_get_range_numobs(range_)

    if status == 0:
        c_dbkey = c_int(dbkey)
        c_oname = c_char_p(oname.encode('UTF-8'))
        c_range = pointer(range_)
        c_strary = (c_char_p * numobs)(*[cast(create_string_buffer(HLI_MAX_STR_LEN + 1), c_char_p)
                                         for _ in range(numobs)])
        c_inlen = (c_int * numobs)(*([HLI_MAX_STR_LEN] * numobs))
        c_outlen = (c_int * numobs)()

        status = chli.LIB.fame_get_strings(c_dbkey, c_oname, c_range, c_strary, c_inlen, c_outlen)

        if status == 0:
            strings = []
            for s in c_strary:
                if s[0] == chli.constants.FSTRNA.value[0]:
                    strings.append('NA')
                elif s[0] == chli.constants.FSTRND.value[0]:
                    strings.append('ND')
                elif s[0] == chli.constants.FSTRNC.value[0]:
                    strings.append('NC')
                else:
                    strings.append(s.decode())

            return status, np.array(strings)
        else:
            return status, np.array([])

    else:
        return status, np.array([])


def _fame_write_values(c_func, dbkey: int, oname: str, range_: c_fame_range, valary: np.ndarray, c_na, c_nc, c_nd, na,
                       nc, nd, c_type) -> int:
    status, numobs = fame_get_range_numobs(range_)

    if status == 0:
        valary[np.argwhere(np.isnan(valary))] = c_nd
        valary[np.argwhere(valary == na)] = c_na
        valary[np.argwhere(valary == nc)] = c_nc
        valary[np.argwhere(valary == nd)] = c_nd

        c_dbkey = c_int(dbkey)
        c_oname = c_char_p(oname.encode('UTF-8'))
        c_range = pointer(range_)
        c_valary = (c_type * numobs)(*valary)

        status = c_func(c_dbkey, c_oname, c_range, c_valary)

    return status


def fame_write_booleans(dbkey: int, oname: str, range_: c_fame_range, valary: np.ndarray) -> int:
    """
    Writes a range of boolean observations to a boolean type series or scalar object.
    """
    valary = valary.astype('i')
    return _fame_write_values(c_func=chli.LIB.fame_write_booleans,
                              dbkey=dbkey,
                              oname=oname,
                              range_=range_,
                              valary=valary,
                              c_na=chli.constants.FBOONA.value,
                              c_nc=chli.constants.FBOONC.value,
                              c_nd=chli.constants.FBOOND.value,
                              na=np.nan,
                              nc=np.nan,
                              nd=np.nan,
                              c_type=c_fame_boolean)


def fame_write_dates(dbkey: int, oname: str, range_: c_fame_range, value_type: int, valary: np.ndarray) -> int:
    """
    Writes a range of date observations to a date type series or scalar object.
    """
    status, numobs = fame_get_range_numobs(range_)

    if status == 0:
        valary = valary.astype('q')
        valary[np.argwhere(np.isnan(valary))] = c_fame_date.in_dll(chli.LIB, 'FDATND').value
        valary[np.argwhere(valary == np.datetime64('NaT'))] = chli.constants.FDATNA
        valary[np.argwhere(valary == np.datetime64('NaT'))] = chli.constants.FDATNC
        valary[np.argwhere(valary == np.datetime64('NaT'))] = chli.constants.FDATND

        c_dbkey = c_int(dbkey)
        c_oname = c_char_p(oname.encode('UTF-8'))
        c_range = pointer(range_)
        c_value_type = c_fame_freq(value_type)
        c_valary = (c_fame_date * numobs)(*valary)

        status = chli.LIB.fame_write_dates(c_dbkey, c_oname, c_range, c_value_type, c_valary)

    return status


def fame_write_numerics(dbkey: int, oname: str, range_: c_fame_range, valary: np.ndarray) -> int:
    """
    Writes a range of numeric observations to a numeric type series or scalar object.
    """
    valary = valary.astype('f')
    return _fame_write_values(c_func=chli.LIB.fame_write_numerics,
                              dbkey=dbkey,
                              oname=oname,
                              range_=range_,
                              valary=valary,
                              c_na=chli.constants.FNUMNA.value,
                              c_nc=chli.constants.FNUMNC.value,
                              c_nd=chli.constants.FNUMND.value,
                              na=np.nan,
                              nc=np.nan,
                              nd=np.nan,
                              c_type=c_fame_numeric)


def fame_write_precisions(dbkey: int, oname: str, range_: c_fame_range, valary: np.ndarray) -> int:
    """
    Writes a range of precision observations to a precision type series or scalar object.
    """
    valary = valary.astype('d')
    return _fame_write_values(c_func=chli.LIB.fame_write_precisions,
                              dbkey=dbkey,
                              oname=oname,
                              range_=range_,
                              valary=valary,
                              c_na=chli.constants.FPRCNA.value,
                              c_nc=chli.constants.FPRCNC.value,
                              c_nd=chli.constants.FPRCND.value,
                              na=np.nan,
                              nc=np.nan,
                              nd=np.nan,
                              c_type=c_fame_precision)


def fame_write_strings(dbkey: int, oname: str, range_: c_fame_range, valary: np.ndarray) -> int:
    """
    Writes a range of string observations to a string type series or scalar object.
    """
    status, numobs = fame_get_range_numobs(range_)

    if status == 0:
        valary = np.char.encode(valary.astype('str'), 'UTF-8')
        valary[np.argwhere(valary == 'NA'.encode('UTF-8'))] = chli.constants.FSTRNA[:]
        valary[np.argwhere(valary == 'NC'.encode('UTF-8'))] = chli.constants.FSTRNC[:]
        valary[np.argwhere(valary == 'ND'.encode('UTF-8'))] = chli.constants.FSTRND[:]

        c_dbkey = c_int(dbkey)
        c_oname = c_char_p(oname.encode('UTF-8'))
        c_range = pointer(range_)
        c_strary = (c_char_p * numobs)(*valary)

        status = chli.LIB.fame_write_strings(c_dbkey, c_oname, c_range, c_strary)

    return status

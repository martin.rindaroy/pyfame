from functools import partial
from random import choices
from typing import Callable, Tuple
from uuid import uuid4

import numpy as np
import pytest

import pyfame


@pytest.fixture
def db_key():
    pyfame.initialize()

    _, key = pyfame.open_work_database()

    yield key

    pyfame.close_database(key)

    pyfame.finish()


def _test_series(db_key: int,
                 f_write: Callable[..., pyfame.Status],
                 f_read: Callable[..., Tuple[pyfame.Status, np.ndarray]],
                 input_values,
                 data_object_type):
    object_name = 'A' + str(uuid4())[:8]
    status, object_name = pyfame.create_object(db_key=db_key,
                                               name=object_name,
                                               class_=pyfame.DataObjectClass.SERIES,
                                               frequency=pyfame.Frequency.CASE,
                                               type_=data_object_type,
                                               basis=pyfame.DataObjectBasis.UNDEFINED,
                                               observed=pyfame.DataObjectObserved.UNDEFINED)
    assert status == pyfame.Status.HSUCC

    status, fame_range = pyfame.init_range(start=0,
                                           frequency=pyfame.Frequency.CASE,
                                           periods=len(input_values))
    assert status == pyfame.Status.HSUCC

    status = f_write(db_key=db_key,
                     name=object_name,
                     fame_range=fame_range,
                     values=input_values)
    assert status == pyfame.Status.HSUCC

    status, output_values = f_read(db_key=db_key,
                                   name=object_name,
                                   fame_range=fame_range)
    assert status == pyfame.Status.HSUCC

    assert all(input_values == output_values)


def test_boolean(db_key):
    input_values = np.array(choices([True, False], k=48))
    data_object_type = pyfame.DataObjectType.BOOLEAN

    _test_series(db_key,
                 pyfame.write_booleans,
                 pyfame.read_booleans,
                 input_values,
                 data_object_type)


def test_date(db_key):
    input_values = ([pyfame.FameTime(2023, 1, 1, x) for x in range(24)]
                    + [pyfame.FameTime(2023, 1, 2, x) for x in range(24)])
    data_object_type = pyfame.Frequency.HOURLY

    _test_series(db_key,
                 partial(pyfame.write_dates, frequency=data_object_type),
                 partial(pyfame.read_dates, frequency=data_object_type),
                 input_values,
                 data_object_type)


def test_numeric(db_key):
    input_values = np.random.random(48).astype('f')
    data_object_type = pyfame.DataObjectType.NUMERIC

    _test_series(db_key,
                 pyfame.write_numerics,
                 pyfame.read_numerics,
                 input_values,
                 data_object_type)


def test_precision(db_key):
    input_values = np.random.random(48).astype('d')
    data_object_type = pyfame.DataObjectType.PRECISION

    _test_series(db_key,
                 pyfame.write_precisions,
                 pyfame.read_precisions,
                 input_values,
                 data_object_type)


def test_string(db_key):
    input_values = np.array(choices(['abc', 'def', 'ghi'], k=48))
    data_object_type = pyfame.DataObjectType.STRING

    _test_series(db_key,
                 pyfame.write_strings,
                 pyfame.read_strings,
                 input_values,
                 data_object_type)

import numpy as np
import pandas as pd
import pytest
from pandas import Timestamp

import pyfame
from pyfame import tz_localize, tz_convert


def test_tz_localize():
    dst_spring = pd.Series(index=pd.date_range(start=Timestamp('2023-03-26'),
                                               end=Timestamp('2023-03-27'),
                                               freq='H',
                                               inclusive='left'),
                           data=np.random.random(24))

    dst_fall = pd.Series(index=pd.date_range(start=Timestamp('2023-10-29'),
                                             end=Timestamp('2023-10-30'),
                                             freq='H',
                                             inclusive='left'),
                         data=np.random.random(24))

    dst_spring_localized = tz_localize(dst_spring, 'Europe/Oslo')
    dst_fall_localized = tz_localize(dst_fall, 'Europe/Oslo')

    assert len(dst_spring_localized) == len(dst_spring) - 1
    assert dst_spring_localized.equals(pd.Series(index=pd.date_range(start=Timestamp('2023-03-26'),
                                                                     end=Timestamp('2023-03-27'),
                                                                     freq='H',
                                                                     inclusive='left',
                                                                     tz='Europe/Oslo'),
                                                 data=[dst_spring[Timestamp('2023-03-26 00:00')],
                                                       dst_spring[Timestamp('2023-03-26 01:00')],
                                                       dst_spring[Timestamp('2023-03-26 03:00')],
                                                       dst_spring[Timestamp('2023-03-26 04:00')],
                                                       dst_spring[Timestamp('2023-03-26 05:00')],
                                                       dst_spring[Timestamp('2023-03-26 06:00')],
                                                       dst_spring[Timestamp('2023-03-26 07:00')],
                                                       dst_spring[Timestamp('2023-03-26 08:00')],
                                                       dst_spring[Timestamp('2023-03-26 09:00')],
                                                       dst_spring[Timestamp('2023-03-26 10:00')],
                                                       dst_spring[Timestamp('2023-03-26 11:00')],
                                                       dst_spring[Timestamp('2023-03-26 12:00')],
                                                       dst_spring[Timestamp('2023-03-26 13:00')],
                                                       dst_spring[Timestamp('2023-03-26 14:00')],
                                                       dst_spring[Timestamp('2023-03-26 15:00')],
                                                       dst_spring[Timestamp('2023-03-26 16:00')],
                                                       dst_spring[Timestamp('2023-03-26 17:00')],
                                                       dst_spring[Timestamp('2023-03-26 18:00')],
                                                       dst_spring[Timestamp('2023-03-26 19:00')],
                                                       dst_spring[Timestamp('2023-03-26 20:00')],
                                                       dst_spring[Timestamp('2023-03-26 21:00')],
                                                       dst_spring[Timestamp('2023-03-26 22:00')],
                                                       dst_spring[Timestamp('2023-03-26 23:00')]]))

    assert len(dst_fall_localized) == len(dst_fall) + 1
    assert dst_fall_localized.equals(pd.Series(index=pd.date_range(start=Timestamp('2023-10-29'),
                                                                   end=Timestamp('2023-10-30'),
                                                                   freq='H',
                                                                   inclusive='left',
                                                                   tz='Europe/Oslo'),
                                               data=[dst_fall[Timestamp('2023-10-29 00:00')],
                                                     dst_fall[Timestamp('2023-10-29 01:00')],
                                                     dst_fall[Timestamp('2023-10-29 02:00')],
                                                     np.nan,
                                                     dst_fall[Timestamp('2023-10-29 03:00')],
                                                     dst_fall[Timestamp('2023-10-29 04:00')],
                                                     dst_fall[Timestamp('2023-10-29 05:00')],
                                                     dst_fall[Timestamp('2023-10-29 06:00')],
                                                     dst_fall[Timestamp('2023-10-29 07:00')],
                                                     dst_fall[Timestamp('2023-10-29 08:00')],
                                                     dst_fall[Timestamp('2023-10-29 09:00')],
                                                     dst_fall[Timestamp('2023-10-29 10:00')],
                                                     dst_fall[Timestamp('2023-10-29 11:00')],
                                                     dst_fall[Timestamp('2023-10-29 12:00')],
                                                     dst_fall[Timestamp('2023-10-29 13:00')],
                                                     dst_fall[Timestamp('2023-10-29 14:00')],
                                                     dst_fall[Timestamp('2023-10-29 15:00')],
                                                     dst_fall[Timestamp('2023-10-29 16:00')],
                                                     dst_fall[Timestamp('2023-10-29 17:00')],
                                                     dst_fall[Timestamp('2023-10-29 18:00')],
                                                     dst_fall[Timestamp('2023-10-29 19:00')],
                                                     dst_fall[Timestamp('2023-10-29 20:00')],
                                                     dst_fall[Timestamp('2023-10-29 21:00')],
                                                     dst_fall[Timestamp('2023-10-29 22:00')],
                                                     dst_fall[Timestamp('2023-10-29 23:00')]]))


def test_tz_convert():
    dst_spring = pd.Series(index=pd.date_range(start=Timestamp('2023-03-26'),
                                               end=Timestamp('2023-03-27'),
                                               freq='H',
                                               inclusive='left',
                                               tz='Europe/Oslo'),
                           data=np.random.random(23))

    dst_fall = pd.Series(index=pd.date_range(start=Timestamp('2023-10-29'),
                                             end=Timestamp('2023-10-30'),
                                             freq='H',
                                             inclusive='left',
                                             tz='Europe/Oslo'),
                         data=np.random.random(25))

    dst_spring_converted = tz_convert(dst_spring, 'Europe/Oslo')
    dst_fall_converted = tz_convert(dst_fall, 'Europe/Oslo')

    assert len(dst_spring_converted) == len(dst_spring) + 1
    assert dst_spring_converted.equals(pd.Series(index=pd.date_range(start=Timestamp('2023-03-26'),
                                                                     end=Timestamp('2023-03-27'),
                                                                     freq='H',
                                                                     inclusive='left'),
                                                 data=[dst_spring[Timestamp('2023-03-26 00:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 01:00', tz='Europe/Oslo')],
                                                       np.nan,
                                                       dst_spring[Timestamp('2023-03-26 03:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 04:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 05:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 06:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 07:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 08:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 09:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 10:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 11:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 12:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 13:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 14:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 15:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 16:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 17:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 18:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 19:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 20:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 21:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 22:00', tz='Europe/Oslo')],
                                                       dst_spring[Timestamp('2023-03-26 23:00', tz='Europe/Oslo')]]))

    assert len(dst_fall_converted) == len(dst_fall) - 1
    assert dst_fall_converted.equals(pd.Series(index=pd.date_range(start=Timestamp('2023-10-29'),
                                                                   end=Timestamp('2023-10-30'),
                                                                   freq='H',
                                                                   inclusive='left'),
                                               data=[dst_fall[Timestamp('2023-10-29 00:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 01:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 02:00+01:00')],
                                                     dst_fall[Timestamp('2023-10-29 03:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 04:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 05:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 06:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 07:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 08:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 09:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 10:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 11:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 12:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 13:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 14:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 15:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 16:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 17:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 18:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 19:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 20:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 21:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 22:00', tz='Europe/Oslo')],
                                                     dst_fall[Timestamp('2023-10-29 23:00', tz='Europe/Oslo')]]))


@pytest.fixture
def setup():
    pyfame.initialize()
    yield
    pyfame.finish()


def test_fame_range_to_pandas_index(setup):
    freq = pyfame.Frequency.CASE
    _, fame_range = pyfame.init_range(start=0, end=5, frequency=freq)
    pandas_index = pyfame.fame_range_to_pandas_index(fame_range)

    assert isinstance(pandas_index, pd.RangeIndex)
    assert len(pandas_index) == pyfame.range_length(fame_range)[1]
    assert pandas_index[0] == fame_range.start
    assert pandas_index[-1] == fame_range.end
    assert fame_range == pyfame.pandas_index_to_fame_range(pandas_index)

    freq = pyfame.Frequency.HOURLY
    _, fame_range = pyfame.init_range(start=0, end=5, frequency=freq)
    pandas_index = pyfame.fame_range_to_pandas_index(fame_range)

    assert isinstance(pandas_index, pd.DatetimeIndex)
    assert len(pandas_index) == pyfame.range_length(fame_range)[1]
    assert pandas_index.freqstr == 'H'
    assert pandas_index[0] == pyfame.fame_time_to_timestamp(pyfame.index_to_time(freq, fame_range.start)[1])
    assert pandas_index[-1] == pyfame.fame_time_to_timestamp(pyfame.index_to_time(freq, fame_range.end)[1])
    assert pyfame.pandas_index_to_fame_range(pandas_index) == fame_range


def test_pandas_index_to_fame_range(setup):
    freq = pyfame.Frequency.CASE
    pandas_index = pd.RangeIndex(start=0, stop=5)
    fame_range = pyfame.pandas_index_to_fame_range(pandas_index)

    assert pyfame.range_length(fame_range)[1] == len(pandas_index)
    assert fame_range.frequency == freq
    assert fame_range.start == pandas_index[0]
    assert fame_range.end == pandas_index[-1]
    assert pyfame.fame_range_to_pandas_index(fame_range).equals(pandas_index)

    freq = pyfame.Frequency.HOURLY
    pandas_index = pd.date_range(start=Timestamp('2020-01-01'), periods=24, freq='H')
    fame_range = pyfame.pandas_index_to_fame_range(pandas_index)

    assert pyfame.range_length(fame_range)[1] == len(pandas_index)
    assert fame_range.frequency == freq
    assert fame_range.start == pyfame.time_to_index(freq, pyfame.timestamp_to_fame_time(pandas_index[0]))[1]
    assert fame_range.end == pyfame.time_to_index(freq, pyfame.timestamp_to_fame_time(pandas_index[-1]))[1]
    assert pyfame.fame_range_to_pandas_index(fame_range).equals(pandas_index)

    # Index type must be RangeIndex of DatetimeIndex
    with pytest.raises(TypeError):
        pyfame.pandas_index_to_fame_range(pd.Index([0, 1, 2]))

    # DatetimeIndex must have frequency
    with pytest.raises(ValueError):
        pyfame.pandas_index_to_fame_range(pd.DatetimeIndex([Timestamp('2020-01-01')]))

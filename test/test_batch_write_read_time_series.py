from random import choices
from typing import Callable, Tuple, Dict

import numpy as np
import pandas as pd
import pytest
from pandas import Timestamp

import pyfame


@pytest.fixture
def db_key():
    pyfame.initialize()

    _, key = pyfame.open_work_database()

    yield key

    pyfame.close_database(key)

    pyfame.finish()


def _test_series(db_key: int,
                 f_write: Callable[..., Dict[str, pyfame.Status]],
                 f_read: Callable[..., Tuple[Dict[str, pyfame.Status], pd.DataFrame]],
                 input_data: pd.DataFrame,
                 data_object_type: pyfame.DataObjectType,
                 data_object_observed: pyfame.DataObjectObserved):
    for object_name in input_data.columns:
        status, object_name = pyfame.create_object(db_key=db_key,
                                                   name=object_name,
                                                   class_=pyfame.DataObjectClass.SERIES,
                                                   frequency=pyfame.Frequency.HOURLY,
                                                   type_=data_object_type,
                                                   basis=pyfame.DataObjectBasis.DAILY,
                                                   observed=data_object_observed)
        assert status == pyfame.Status.HSUCC

    status = f_write(db_key=db_key, data=input_data)
    for s in status.values():
        assert s == pyfame.Status.HSUCC

    fame_range = pyfame.pandas_index_to_fame_range(input_data.index)

    status, output_data = f_read(db_key=db_key, names=input_data.columns, fame_range=fame_range)
    for s in status.values():
        assert s == pyfame.Status.HSUCC

    assert input_data.equals(output_data)


def test_boolean(db_key):
    index = pd.date_range(start=Timestamp('2020-01-01'), periods=5, freq='H')
    input_data = pd.DataFrame(data={'bool_a': np.array(choices([True, False], k=5)),
                                    'bool_b': np.array(choices([True, False], k=5))},
                              index=index)
    data_object_type = pyfame.DataObjectType.BOOLEAN
    data_object_observed = pyfame.DataObjectObserved.UNDEFINED

    _test_series(db_key,
                 pyfame.batch_write_booleans,
                 pyfame.batch_read_booleans,
                 input_data,
                 data_object_type,
                 data_object_observed)


def test_numeric(db_key):
    index = pd.date_range(start=Timestamp('2020-01-01'), periods=5, freq='H')
    input_data = pd.DataFrame(data={'num_a': np.random.random(5).astype('f'),
                                    'num_b': np.random.random(5).astype('f')},
                              index=index)
    data_object_type = pyfame.DataObjectType.NUMERIC
    data_object_observed = pyfame.DataObjectObserved.AVERAGED

    _test_series(db_key,
                 pyfame.batch_write_numerics,
                 pyfame.batch_read_numerics,
                 input_data,
                 data_object_type,
                 data_object_observed)


def test_precision(db_key):
    index = pd.date_range(start=Timestamp('2020-01-01'), periods=5, freq='H')
    input_data = pd.DataFrame(data={'prec_a': np.random.random(5).astype('d'),
                                    'prec_b': np.random.random(5).astype('d')},
                              index=index)
    data_object_type = pyfame.DataObjectType.PRECISION
    data_object_observed = pyfame.DataObjectObserved.AVERAGED

    _test_series(db_key,
                 pyfame.batch_write_precisions,
                 pyfame.batch_read_precisions,
                 input_data,
                 data_object_type,
                 data_object_observed)


def test_string(db_key):
    index = pd.date_range(start=Timestamp('2020-01-01'), periods=5, freq='H')
    input_data = pd.DataFrame(data={'str_a': ['a1', 'b1', 'c1', 'd2', 'e1'],
                                    'str_b': ['a2', 'b2', 'c2', 'd2', 'e2']},
                              index=index)
    data_object_type = pyfame.DataObjectType.STRING
    data_object_observed = pyfame.DataObjectObserved.UNDEFINED

    _test_series(db_key,
                 pyfame.batch_write_strings,
                 pyfame.batch_read_strings,
                 input_data,
                 data_object_type,
                 data_object_observed)

import pytest

import pyfame


@pytest.fixture
def db_key():
    pyfame.initialize()

    _, key = pyfame.open_work_database()

    yield key

    pyfame.close_database(key)

    pyfame.finish()


def test_init_range(db_key):
    periods = 24
    frequency = pyfame.Frequency.HOURLY
    start_time = pyfame.FameTime(2023, 1, 1, 0)
    end_time = pyfame.FameTime(2023, 1, 1, 23)

    status, fame_range = pyfame.init_range(start=start_time,
                                           frequency=frequency,
                                           periods=periods)

    assert status == pyfame.Status.HSUCC
    assert pyfame.index_to_time(frequency, fame_range.start)[1] == start_time
    assert pyfame.index_to_time(frequency, fame_range.end)[1] == end_time
    assert fame_range.frequency == frequency

    status, fame_range = pyfame.init_range(start=pyfame.time_to_index(frequency, start_time)[1],
                                           frequency=frequency,
                                           periods=periods)

    assert status == pyfame.Status.HSUCC
    assert pyfame.index_to_time(frequency, fame_range.start)[1] == start_time
    assert pyfame.index_to_time(frequency, fame_range.end)[1] == end_time
    assert fame_range.frequency == frequency

    status, fame_range = pyfame.init_range(end=end_time,
                                           frequency=frequency,
                                           periods=periods)

    assert status == pyfame.Status.HSUCC
    assert pyfame.index_to_time(frequency, fame_range.start)[1] == start_time
    assert pyfame.index_to_time(frequency, fame_range.end)[1] == end_time
    assert fame_range.frequency == frequency

    status, fame_range = pyfame.init_range(end=pyfame.time_to_index(frequency, end_time)[1],
                                           frequency=frequency,
                                           periods=periods)

    assert status == pyfame.Status.HSUCC
    assert pyfame.index_to_time(frequency, fame_range.start)[1] == start_time
    assert pyfame.index_to_time(frequency, fame_range.end)[1] == end_time
    assert fame_range.frequency == frequency

    status, fame_range = pyfame.init_range(start=start_time,
                                           end=end_time,
                                           frequency=frequency)

    assert status == pyfame.Status.HSUCC
    assert pyfame.index_to_time(frequency, fame_range.start)[1] == start_time
    assert pyfame.index_to_time(frequency, fame_range.end)[1] == end_time
    assert fame_range.frequency == frequency

    status, fame_range = pyfame.init_range(start=pyfame.time_to_index(frequency, start_time)[1],
                                           end=pyfame.time_to_index(frequency, end_time)[1],
                                           frequency=frequency)

    assert status == pyfame.Status.HSUCC
    assert pyfame.index_to_time(frequency, fame_range.start)[1] == start_time
    assert pyfame.index_to_time(frequency, fame_range.end)[1] == end_time
    assert fame_range.frequency == frequency

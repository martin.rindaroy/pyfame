from functools import partial
from random import choices
from typing import Callable, Tuple
from uuid import uuid4

import numpy as np
import pytest

import pyfame


@pytest.fixture
def db_key():
    pyfame.initialize()

    _, key = pyfame.open_work_database()

    yield key

    pyfame.close_database(key)

    pyfame.finish()


def _test_series(db_key: int,
                 f_write: Callable[..., pyfame.Status],
                 f_read: Callable[..., Tuple[pyfame.Status, np.ndarray]],
                 input_values,
                 data_object_type,
                 data_object_observed):
    object_name = 'A' + str(uuid4())[:8]
    status, object_name = pyfame.create_object(db_key=db_key,
                                               name=object_name,
                                               class_=pyfame.DataObjectClass.SERIES,
                                               frequency=pyfame.Frequency.HOURLY,
                                               type_=data_object_type,
                                               basis=pyfame.DataObjectBasis.DAILY,
                                               observed=data_object_observed)
    assert status == pyfame.Status.HSUCC

    status, fame_range = pyfame.init_range(start=pyfame.FameTime(2023, 3, 25, 12),
                                           frequency=pyfame.Frequency.HOURLY,
                                           periods=len(input_values))
    assert status == pyfame.Status.HSUCC

    status = f_write(db_key=db_key,
                     name=object_name,
                     fame_range=fame_range,
                     values=input_values)
    assert status == pyfame.Status.HSUCC

    status, output_values = f_read(db_key=db_key,
                                   name=object_name,
                                   fame_range=fame_range)
    assert status == pyfame.Status.HSUCC

    assert all(input_values == output_values)


def test_boolean(db_key):
    input_values = np.array(choices([True, False], k=48))
    data_object_type = pyfame.DataObjectType.BOOLEAN
    data_object_observed = pyfame.DataObjectObserved.UNDEFINED

    _test_series(db_key,
                 pyfame.write_booleans,
                 pyfame.read_booleans,
                 input_values,
                 data_object_type,
                 data_object_observed)


def test_date(db_key):
    input_values = ([pyfame.FameTime(2023, 1, 1, x) for x in range(24)]
                    + [pyfame.FameTime(2023, 1, 2, x) for x in range(24)])
    data_object_type = pyfame.DataObjectType.DATE
    data_object_observed = pyfame.DataObjectObserved.UNDEFINED

    _test_series(db_key,
                 partial(pyfame.write_dates, frequency=pyfame.Frequency.HOURLY),
                 partial(pyfame.read_dates, frequency=pyfame.Frequency.HOURLY),
                 input_values,
                 data_object_type,
                 data_object_observed)


def test_numeric(db_key):
    input_values = np.random.random(48).astype('f')
    data_object_type = pyfame.DataObjectType.NUMERIC
    data_object_observed = pyfame.DataObjectObserved.AVERAGED

    _test_series(db_key,
                 pyfame.write_numerics,
                 pyfame.read_numerics,
                 input_values,
                 data_object_type,
                 data_object_observed)


def test_precision(db_key):
    input_values = np.random.random(48).astype('d')
    data_object_type = pyfame.DataObjectType.PRECISION
    data_object_observed = pyfame.DataObjectObserved.AVERAGED

    _test_series(db_key,
                 pyfame.write_precisions,
                 pyfame.read_precisions,
                 input_values,
                 data_object_type,
                 data_object_observed)


def test_string(db_key):
    input_values = np.array(choices(['abc', 'def', 'ghi'], k=48))
    data_object_type = pyfame.DataObjectType.STRING
    data_object_observed = pyfame.DataObjectObserved.UNDEFINED

    _test_series(db_key,
                 pyfame.write_strings,
                 pyfame.read_strings,
                 input_values,
                 data_object_type,
                 data_object_observed)

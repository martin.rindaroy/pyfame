import numpy as np

import pyfame
from config import PREPROD_HOST, WRITE_SERVICE

status = pyfame.initialize()

status, connection_key = pyfame.open_connection(service=WRITE_SERVICE,
                                                hostname=PREPROD_HOST)

status, db_key = pyfame.open_remote_database(db_name='nordic_sim',
                                             mode=pyfame.DatabaseAccessMode.WRITE,
                                             connection_key=connection_key)

status, fame_range = pyfame.init_range(start=pyfame.FameTime(2023, 10, 30),
                                       frequency=pyfame.Frequency.HOURLY,
                                       periods=24)

status = pyfame.write_precisions(db_key=db_key,
                                 name='PRODUCTION.CHP.SE1.MWH.H.H',
                                 fame_range=fame_range,
                                 values=np.array([70] * fame_range.periods))

status = pyfame.commit(connection_key=connection_key)

status = pyfame.close_database(db_key=db_key)

status = pyfame.close_connection(connection_key=connection_key)

status = pyfame.finish()

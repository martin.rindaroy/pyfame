import numpy as np

import pyfame
from config import PREPROD_HOST, READ_SERVICE

_ = pyfame.initialize()

_, connection_key = pyfame.open_connection(service=READ_SERVICE,
                                           hostname=PREPROD_HOST)

_, db_key = pyfame.open_remote_database(db_name='nordic_sim',
                                        mode=pyfame.DatabaseAccessMode.READ,
                                        connection_key=connection_key)

_, info = pyfame.get_object_info(db_key=db_key,
                                 name='info._.name_in_nln.kpp._._.c')

_, fame_range = pyfame.init_range(start=info['first_index'],
                                  end=info['last_index'],
                                  frequency=info['frequency'])

_, name_in_nln = pyfame.read_strings(db_key=db_key,
                                     name='info._.name_in_nln.kpp._._.c',
                                     fame_range=fame_range)

_, modeled_in_mini = pyfame.read_booleans(db_key=db_key,
                                          name='info._.modeled_in_mini.kpp._._.c',
                                          fame_range=fame_range)

_, affected_price_areas = pyfame.read_strings(db_key=db_key,
                                              name='info._.affected_price_areas.kpp._._.c',
                                              fame_range=fame_range)

names = list(map(lambda x: f'MARGINAL_COST.{x[0]}.{x[1]}.EUR.MWH.H',
                 zip(name_in_nln[np.argwhere(modeled_in_mini == True)].squeeze(),
                     affected_price_areas[np.argwhere(modeled_in_mini == True)].squeeze())))

_, fame_range = pyfame.init_range(start=pyfame.FameTime(2023, 3, 25),
                                  frequency=pyfame.Frequency.HOURLY,
                                  periods=3 * 24)

_, df = pyfame.batch_read_precisions(db_key=db_key,
                                     names=names,
                                     fame_range=fame_range)

df = pyfame.tz_localize(df, 'Europe/Oslo')

_ = pyfame.close_database(db_key=db_key)

_ = pyfame.close_connection(connection_key=connection_key)

_ = pyfame.finish()

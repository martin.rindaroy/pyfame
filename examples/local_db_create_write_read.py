import numpy as np

import pyfame

status = pyfame.initialize()

status, db_key, db_name = pyfame.open_local_database(db_name=f'local_test_{np.random.randint(1000)}',
                                                     mode=pyfame.DatabaseAccessMode.CREATE)
status = pyfame.close_database(db_key)

status, db_key, db_name = pyfame.open_local_database(db_name=db_name, mode=pyfame.DatabaseAccessMode.UPDATE)

status, object_name = pyfame.create_object(db_key=db_key,
                                           name='my.new.test',
                                           class_=pyfame.DataObjectClass.SERIES,
                                           frequency=pyfame.Frequency.HOURLY,
                                           type_=pyfame.DataObjectType.PRECISION,
                                           basis=pyfame.DataObjectBasis.DAILY,
                                           observed=pyfame.DataObjectObserved.AVERAGED)

status, fame_range = pyfame.init_range(start=pyfame.FameTime(2023, 10, 30),
                                       frequency=pyfame.Frequency.HOURLY,
                                       periods=24)

status = pyfame.write_precisions(db_key=db_key,
                                 name=object_name,
                                 fame_range=fame_range,
                                 values=np.random.random(24))

status, values = pyfame.read_precisions(db_key=db_key,
                                        name=object_name,
                                        fame_range=fame_range)

status = pyfame.close_database(db_key)

status = pyfame.finish()

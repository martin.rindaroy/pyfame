from pyfame.enums import Frequency

PANDAS_FREQ_TO_FAME_FREQ = {
    'L': Frequency.MILLISECONDLY,
    'T': Frequency.MINUTELY,
    'H': Frequency.HOURLY,
    'D': Frequency.DAILY,
    'B': Frequency.BUSINESS,
    'W-SUN': Frequency.WEEKLY_SUNDAY,
    'W-MON': Frequency.WEEKLY_MONDAY,
    'W-TUE': Frequency.WEEKLY_TUESDAY,
    'W-WED': Frequency.WEEKLY_WEDNESDAY,
    'W-THU': Frequency.WEEKLY_THURSDAY,
    'W-FRI': Frequency.WEEKLY_FRIDAY,
    'W-SAT': Frequency.WEEKLY_SATURDAY,
    'M': Frequency.MONTHLY,
    'Q-OCT': Frequency.QUARTERLY_OCTOBER,
    'Q-NOV': Frequency.QUARTERLY_NOVEMBER,
    'Q-DEC': Frequency.QUARTERLY_DECEMBER,
    'A-JAN': Frequency.ANNUAL_JANUARY,
    'A-FEB': Frequency.ANNUAL_FEBRUARY,
    'A-MAR': Frequency.ANNUAL_MARCH,
    'A-APR': Frequency.ANNUAL_APRIL,
    'A-MAY': Frequency.ANNUAL_MAY,
    'A-JUN': Frequency.ANNUAL_JUNE,
    'A-JUL': Frequency.ANNUAL_JULY,
    'A-AUG': Frequency.ANNUAL_AUGUST,
    'A-SEP': Frequency.ANNUAL_SEPTEMBER,
    'A-OCT': Frequency.ANNUAL_OCTOBER,
    'A-NOV': Frequency.ANNUAL_NOVEMBER,
    'A-DEC': Frequency.ANNUAL_DECEMBER,
}

FAME_FREQ_TO_PANDAS_FREQ = {value: key for key, value in PANDAS_FREQ_TO_FAME_FREQ.items()}

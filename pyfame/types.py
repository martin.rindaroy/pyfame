from dataclasses import dataclass

from pyfame.enums import Frequency

DatabaseKey = int
ConnectionKey = int
WildKey = int
DatabaseName = str
ObjectName = str
WildName = str


@dataclass
class FameRange:
    frequency: Frequency
    start: int
    end: int


@dataclass
class FameTime:
    year: int
    month: int
    day: int
    hour: int = 0
    minute: int = 0
    second: int = 0
    millisecond: int = 0

from typing import Tuple, List, Dict

import numpy as np
import pandas as pd
from pandas import Timestamp, date_range, DatetimeIndex, RangeIndex

import pyfame
from pyfame.enums import Status, Frequency
from pyfame.functions import init_wildcard, next_wildcard, free_wildcard, index_to_time, time_to_index
from pyfame.maps import FAME_FREQ_TO_PANDAS_FREQ, PANDAS_FREQ_TO_FAME_FREQ
from pyfame.types import WildName, FameTime, FameRange


def search(db_key, wild_name: WildName) -> Tuple[Status, List[Dict]]:
    status, wild_key = init_wildcard(db_key=db_key, wild_name=wild_name)
    if Status(status) != Status.HSUCC:
        return status, []

    result = []
    while True:
        status, data = next_wildcard(wild_key)
        if Status(status) == Status.HSUCC:
            result.append(data)
        else:
            break

    status = free_wildcard(wild_key)

    return status, result


def fame_frequency_to_pandas_frequency(frequency: Frequency) -> str:
    return FAME_FREQ_TO_PANDAS_FREQ[frequency]


def pandas_frequency_to_fame_frequency(frequency: str) -> Frequency:
    return PANDAS_FREQ_TO_FAME_FREQ[frequency]


def fame_time_to_timestamp(fame_time: FameTime) -> Timestamp:
    return Timestamp(year=fame_time.year,
                     month=fame_time.month,
                     day=fame_time.day,
                     hour=fame_time.hour,
                     minute=fame_time.minute,
                     second=fame_time.second,
                     microsecond=int(fame_time.millisecond * 1000))


def timestamp_to_fame_time(timestamp: Timestamp) -> FameTime:
    return FameTime(year=timestamp.year,
                    month=timestamp.month,
                    day=timestamp.day,
                    hour=timestamp.hour,
                    minute=timestamp.minute,
                    second=timestamp.second,
                    millisecond=int(timestamp.microsecond / 1000))


def fame_range_to_pandas_index(fame_range: FameRange) -> RangeIndex | DatetimeIndex:
    if fame_range.frequency == pyfame.Frequency.CASE:
        return pd.RangeIndex(start=fame_range.start, stop=fame_range.end + 1)
    else:
        return date_range(start=fame_time_to_timestamp(index_to_time(fame_range.frequency, fame_range.start)[1]),
                          end=fame_time_to_timestamp(index_to_time(fame_range.frequency, fame_range.end)[1]),
                          freq=fame_frequency_to_pandas_frequency(fame_range.frequency))


def pandas_index_to_fame_range(index: RangeIndex | DatetimeIndex) -> FameRange:
    if isinstance(index, DatetimeIndex):
        if index.freqstr is None:
            raise ValueError("DatetimeIndex must specify a frequency")
        else:
            frequency = pandas_frequency_to_fame_frequency(index.freqstr)
            return FameRange(frequency=frequency,
                             start=time_to_index(frequency, timestamp_to_fame_time(index[0]))[1],
                             end=time_to_index(frequency, timestamp_to_fame_time(index[-1]))[1])
    elif isinstance(index, RangeIndex):
        return FameRange(frequency=Frequency.CASE,
                         start=index[0],
                         end=index[-1])
    else:
        raise TypeError("Input index must be of type RangeIndex or DatetimeIndex")


def to_series(values: np.ndarray, fame_range: FameRange, name: str = None, db_tz: str = None) -> pd.Series:
    series = pd.Series(data=values, index=fame_range_to_pandas_index(fame_range), name=name)

    if db_tz is None:
        return series
    else:
        return tz_localize(series, db_tz)


def to_dataframe(values: Dict[str, np.ndarray],
                 fame_range: FameRange,
                 db_tz: str = None) -> pd.DataFrame:
    series = pd.DataFrame(data=values, index=fame_range_to_pandas_index(fame_range))

    if db_tz is None:
        return series
    else:
        return tz_localize(series, db_tz)


def tz_localize(data: pd.DataFrame | pd.Series, db_tz: str) -> pd.DataFrame | pd.Series:
    return (data
            .tz_localize(db_tz, ambiguous=np.array([True] * len(data)), nonexistent='NaT')
            .resample(data.index.freqstr)
            .last()
            .asfreq(data.index.freqstr))


def tz_convert(data: pd.DataFrame | pd.Series, db_tz: str = None) -> pd.DataFrame | pd.Series:
    if db_tz is not None:
        data = data.tz_convert(db_tz)

    return (data
            .tz_localize(None)
            .resample(data.index.freqstr)
            .last()
            .asfreq(data.index.freqstr))

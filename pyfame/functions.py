from functools import partial
from typing import Optional, Tuple, List, Dict, Callable, Any

import numpy as np

import chli
from chli.types import c_fame_range
from pyfame.enums import (DatabaseAccessMode, DataObjectBasis, DataObjectObserved, DataObjectClass, DataObjectType,
                          Frequency, Status, BooleanFlag)
from pyfame.types import DatabaseKey, ConnectionKey, DatabaseName, ObjectName, WildKey, WildName, FameRange, FameTime


def initialize() -> Status:
    status = chli.cfmini()
    return Status(status)


def finish() -> Status:
    status = chli.cfmfin()
    return Status(status)


def open_work_database() -> Tuple[Status, DatabaseKey]:
    status, db_key = chli.cfmopwk()
    return Status(status), db_key


def open_local_database(db_name: DatabaseName, mode: DatabaseAccessMode) -> Tuple[Status, DatabaseKey, DatabaseName]:
    status, db_key, db_name = chli.cfmopdb(dbname=db_name, mode=mode.value)
    return Status(status), db_key, db_name


def open_remote_database(db_name: DatabaseName,
                         mode: DatabaseAccessMode,
                         connection_key: ConnectionKey
                         ) -> Tuple[Status, DatabaseKey]:
    status, db_key = chli.cfmopdc(dbname=db_name, mode=mode.value, connkey=connection_key)
    return Status(status), db_key


def open_analytical_channel(connection_key: ConnectionKey) -> Tuple[Status, DatabaseKey]:
    status, db_key = chli.cfmoprc(connkey=connection_key)
    return Status(status), db_key


def close_database(db_key: DatabaseKey) -> Status:
    status = chli.cfmcldb(dbkey=db_key)
    return Status(status)


def open_connection(service: str,
                    hostname: str,
                    username: str = '',
                    password: str = ''
                    ) -> Tuple[Status, ConnectionKey]:
    status, connection_key = chli.cfmopcn(service=service,
                                          hostname=hostname,
                                          username=username,
                                          password=password)

    return Status(status), connection_key


def close_connection(connection_key: ConnectionKey) -> Status:
    status = chli.cfmclcn(connkey=connection_key)
    return Status(status)


def post(db_key: DatabaseKey) -> Status:
    status = chli.cfmpodb(dbkey=db_key)
    return Status(status)


def commit(connection_key: ConnectionKey) -> Status:
    status = chli.cfmcmmt(connkey=connection_key)
    return Status(status)


def abort(connection_key: ConnectionKey) -> Status:
    status = chli.cfmabrt(connkey=connection_key)
    return Status(status)


def create_object(db_key: DatabaseKey,
                  name: ObjectName,
                  class_: DataObjectClass,
                  frequency: Frequency,
                  type_: DataObjectType,
                  basis: DataObjectBasis,
                  observed: DataObjectObserved) -> Tuple[Status, ObjectName]:
    if type_ == DataObjectType.DATE:
        type_ = frequency

    status, name = chli.cfmnwob(dbkey=db_key,
                                objnam=name,
                                class_=class_.value,
                                freq=frequency.value,
                                type_=type_.value,
                                basis=basis.value,
                                observ=observed.value)

    return Status(status), name


def delete_object(db_key: DatabaseKey, name: ObjectName) -> Tuple[Status, ObjectName]:
    status, name = chli.cfmdlob(dbkey=db_key, objnam=name)
    return Status(status), name


def set_object_aliases(db_key: DatabaseKey,
                       name: ObjectName,
                       aliases: List[str]
                       ) -> Tuple[Status, ObjectName]:
    status, name = chli.cfmsali(dbkey=db_key, objnam=name, aliass=','.join(aliases))
    return Status(status), name


def remote_evaluation(remote_db_key: DatabaseKey,
                      expression: str,
                      options: str,
                      local_db_key: DatabaseKey,
                      local_name: ObjectName) -> Status:
    status = chli.cfmrmev(dbkey=remote_db_key,
                          expr=expression,
                          optns=options,
                          wdbkey=local_db_key,
                          objnam=local_name)
    return Status(status)


def get_object_info(db_key: DatabaseKey, name: ObjectName) -> Tuple[Status, Dict]:
    status, *data = chli.fame_info(dbkey=db_key, oname=name)

    if Status(status) != Status.HSUCC:
        return Status(status), {}

    return (Status(status), {
        'class': DataObjectClass(data[0]),
        'type': DataObjectType(data[1]),
        'frequency': Frequency(data[2]),
        'first_index': index_to_time(Frequency(data[2]), data[3])[1]
        if Frequency(data[2]) != Frequency.CASE else data[3],
        'last_index': index_to_time(Frequency(data[2]), data[4])[1]
        if Frequency(data[2]) != Frequency.CASE else data[4],
        'basis': DataObjectBasis(data[5]),
        'observed': DataObjectObserved(data[6]),
        'created': index_to_time(Frequency.MILLISECONDLY, data[7])[1],
        'modified': index_to_time(Frequency.MILLISECONDLY, data[8])[1],
        'description': str(data[9]),
        'documentation': str(data[11])
    })


def get_database_attributes(db_key: DatabaseKey) -> Tuple[Status, Dict]:
    status, *data = chli.cfmgdba(dbkey=db_key)

    if Status(status) != Status.HSUCC:
        return Status(status), {}

    return (Status(status), {
        'created': FameTime(year=data[0], month=data[1], day=data[2]),
        'modified': FameTime(year=data[3], month=data[4], day=data[5]),
        'description': data[6],
        'documentation': data[7]
    })


def set_database_documentation(db_key: DatabaseKey, documentation: str) -> Status:
    status = chli.cfmddoc(dbkey=db_key, doc=documentation)
    return Status(status)


def set_database_description(db_key: DatabaseKey, description: str) -> Status:
    status = chli.cfmddes(dbkey=db_key, desc=description)
    return Status(status)


def set_object_documentation(db_key: DatabaseKey,
                             name: ObjectName,
                             documentation: str
                             ) -> Tuple[Status, ObjectName]:
    status, name = chli.cfmsdoc(dbkey=db_key, objnam=name, doc=documentation)
    return Status(status), name


def set_object_description(db_key: DatabaseKey,
                           name: ObjectName,
                           description: str
                           ) -> Tuple[Status, ObjectName]:
    status, name = chli.cfmsdes(dbkey=db_key, objnam=name, desc=description)
    return Status(status), name


def init_wildcard(db_key: DatabaseKey,
                  wild_name: WildName,
                  wild_only: BooleanFlag = BooleanFlag.NO,
                  wild_start: str = ''
                  ) -> Tuple[Status, WildKey]:
    status, wild_key = chli.fame_init_wildcard(dbkey=db_key,
                                               wildname=wild_name,
                                               wildonly=wild_only.value,
                                               wildstart=wild_start)

    return Status(status), wild_key


def next_wildcard(wild_key: WildKey) -> Tuple[Status, Dict]:
    status, *data = chli.fame_get_next_wildcard(wildkey=wild_key)

    if Status(status) != Status.HSUCC:
        return Status(status), {}
    else:
        return (Status(status), {
            'name': data[0],
            'class': DataObjectClass(data[1]),
            'type': DataObjectType(data[2])
            if data[2] in set(map(lambda x: x.value, DataObjectType)) else Frequency(data[2]),
            'frequency': Frequency(data[3]),
            'start': index_to_time(Frequency(data[3]), data[4])[1],
            'end': index_to_time(Frequency(data[3]), data[5])[1]
        })


def free_wildcard(wild_key: WildKey) -> Status:
    status = chli.fame_free_wildcard(wildkey=wild_key)
    return Status(status)


def fame(command: str) -> Status:
    status = chli.cfmfame(cmd=command)
    return Status(status)


def set_option(option: str, value: str) -> Tuple[Status, str, str]:
    status, option, value = chli.cfmsopt(option, value)
    return Status(status), option, value


def init_range(start: FameTime | int = None,
               end: FameTime | int = None,
               frequency: Frequency = None,
               periods: int = None
               ) -> Tuple[Status, Optional[FameRange]]:
    if sum(x is not None for x in [start, frequency, periods]) and end is None:
        start_index = time_to_index(frequency, start)[1] if isinstance(start, FameTime) else start

        status, range_ = chli.fame_init_range_from_start_numobs(freq=frequency.value, start=start_index, numobs=periods)

    elif sum(x is not None for x in [end, frequency, periods]) and start is None:
        end_index = time_to_index(frequency, end)[1] if isinstance(end, FameTime) else end

        status, range_ = chli.fame_init_range_from_end_numobs(freq=frequency.value, end=end_index, numobs=periods)

    elif sum(x is not None for x in [start, frequency, end]) and periods is None:
        start_index = time_to_index(frequency, start)[1] if isinstance(start, FameTime) else start
        end_index = time_to_index(frequency, end)[1] if isinstance(end, FameTime) else end

        status, range_ = chli.fame_init_range_from_indexes(freq=frequency.value, start=start_index, end=end_index)
    else:
        raise ValueError(
            "Of the four parameters the following combinations are allowed:\n "
            "[start, frequency, periods] is not None and end is None\n"
            "[end, frequency, periods] is not None and start is None\n"
            "[start, frequency, end] is not None and periods is None"
        )

    if Status(status) != Status.HSUCC:
        return Status(status), None
    else:
        freq = Frequency(range_.r_freq)
        return Status(status), FameRange(frequency=freq,
                                         start=range_.r_start,
                                         end=range_.r_end)


def range_length(fame_range: FameRange) -> Tuple[Status, Optional[int]]:
    status, length = chli.fame_get_range_numobs(c_fame_range(r_freq=fame_range.frequency.value,
                                                             r_start=fame_range.start,
                                                             r_end=fame_range.end))

    if Status(status) != Status.HSUCC:
        return Status(status), None
    else:
        return Status(status), length


def index_to_time(frequency: Frequency, index: int) -> Tuple[Status, Optional[FameTime]]:
    status, *fame_time = chli.fame_index_to_time(frequency.value, index)

    if Status(status) != Status.HSUCC:
        return Status(status), None
    else:
        return Status(status), FameTime(year=fame_time[0],
                                        month=fame_time[1],
                                        day=fame_time[2],
                                        hour=fame_time[3],
                                        minute=fame_time[4],
                                        second=fame_time[5],
                                        millisecond=fame_time[6])


def time_to_index(frequency: Frequency, fame_time: FameTime) -> Tuple[Status, Optional[int]]:
    status, index = chli.fame_time_to_index(freq=frequency.value,
                                            year=fame_time.year,
                                            month=fame_time.month,
                                            day=fame_time.day,
                                            hour=fame_time.hour,
                                            minute=fame_time.minute,
                                            second=fame_time.second,
                                            millisecond=fame_time.millisecond)

    if Status(status) != Status.HSUCC:
        return Status(status), None
    else:
        return Status(status), index


def read_booleans(db_key: DatabaseKey,
                  name: ObjectName,
                  fame_range: FameRange
                  ) -> Tuple[Status, Optional[np.ndarray]]:
    status, values = _read(f=chli.fame_get_booleans, db_key=db_key, name=name, fame_range=fame_range)

    if status == Status.HSUCC:
        return status, values.astype(bool)
    else:
        return status, None


def write_booleans(db_key: DatabaseKey,
                   name: ObjectName,
                   fame_range: FameRange,
                   values: np.ndarray
                   ) -> Status:
    return _write(f=chli.fame_write_booleans,
                  db_key=db_key,
                  name=name,
                  fame_range=fame_range,
                  values=values)


def read_dates(db_key: DatabaseKey,
               name: ObjectName,
               fame_range: FameRange,
               frequency: Frequency
               ) -> Tuple[Status, Optional[np.ndarray]]:
    status, values = _read(f=chli.fame_get_dates,
                           db_key=db_key,
                           name=name,
                           fame_range=fame_range)

    if status != Status.HSUCC:
        return Status(status), None
    else:
        values = np.array(list(map(lambda x: index_to_time(frequency, x)[1], values)))
        return Status(status), values.astype(FameTime)


def write_dates(db_key: DatabaseKey,
                name: ObjectName,
                fame_range: FameRange,
                frequency: Frequency,
                values: np.ndarray
                ) -> Status:
    values = np.array(list(map(lambda x: time_to_index(frequency, x)[1], values)))

    return _write(f=partial(chli.fame_write_dates, value_type=frequency.value),
                  db_key=db_key,
                  name=name,
                  fame_range=fame_range,
                  values=values)


def read_numerics(db_key: DatabaseKey,
                  name: ObjectName,
                  fame_range: FameRange
                  ) -> Tuple[Status, Optional[np.ndarray]]:
    status, values = _read(f=chli.fame_get_numerics, db_key=db_key, name=name, fame_range=fame_range)

    if status == Status.HSUCC:
        return status, values.astype('f')
    else:
        return status, None


def write_numerics(db_key: DatabaseKey,
                   name: ObjectName,
                   fame_range: FameRange,
                   values: np.ndarray
                   ) -> Status:
    return _write(f=chli.fame_write_numerics,
                  db_key=db_key,
                  name=name,
                  fame_range=fame_range,
                  values=values)


def read_precisions(db_key: DatabaseKey,
                    name: ObjectName,
                    fame_range: FameRange
                    ) -> Tuple[Status, Optional[np.ndarray]]:
    status, values = _read(f=chli.fame_get_precisions, db_key=db_key, name=name, fame_range=fame_range)

    if status == Status.HSUCC:
        return status, values.astype('d')
    else:
        return status, None


def write_precisions(db_key: DatabaseKey,
                     name: ObjectName,
                     fame_range: FameRange,
                     values: np.ndarray
                     ) -> Status:
    return _write(f=chli.fame_write_precisions,
                  db_key=db_key,
                  name=name,
                  fame_range=fame_range,
                  values=values)


def read_strings(db_key: DatabaseKey,
                 name: ObjectName,
                 fame_range: FameRange
                 ) -> Tuple[Status, Optional[np.ndarray]]:
    status, values = _read(f=chli.fame_get_strings, db_key=db_key, name=name, fame_range=fame_range)

    if status == Status.HSUCC:
        return status, values.astype(str)
    else:
        return status, None


def write_strings(db_key: DatabaseKey,
                  name: ObjectName,
                  fame_range: FameRange,
                  values: np.ndarray
                  ) -> Status:
    return _write(f=chli.fame_write_strings,
                  db_key=db_key,
                  name=name,
                  fame_range=fame_range,
                  values=values)


def _read(f: Callable[[DatabaseKey, ObjectName, Any], Tuple[int, np.ndarray]],
          db_key: DatabaseKey,
          name: ObjectName,
          fame_range: FameRange
          ) -> Tuple[Status, Optional[np.ndarray]]:
    range_ = c_fame_range(r_freq=fame_range.frequency.value,
                          r_start=fame_range.start,
                          r_end=fame_range.end)

    status, values = f(db_key, name, range_)

    if Status(status) != Status.HSUCC:
        return Status(status), None
    else:
        return Status(status), values


def _write(f,
           db_key: DatabaseKey,
           name: ObjectName,
           fame_range: FameRange,
           values: np.ndarray
           ) -> Status:
    range_ = c_fame_range(r_freq=fame_range.frequency.value,
                          r_start=fame_range.start,
                          r_end=fame_range.end)

    status = f(dbkey=db_key, oname=name, range_=range_, valary=values)

    return Status(status)

from typing import List, Tuple, Dict, Callable

import pandas as pd

import pyfame
from pyfame.enums import Status
from pyfame.types import DatabaseKey, ObjectName, FameRange


def _batch_read(f: Callable,
                db_key: DatabaseKey,
                names: List[ObjectName] | ObjectName,
                fame_range: FameRange
                ) -> Tuple[Dict[ObjectName, Status], pd.DataFrame]:
    if isinstance(names, ObjectName):
        names = [names]

    status = {}
    values = {}
    for name in names:
        s, v = f(db_key=db_key, name=name, fame_range=fame_range)

        status[name] = s

        if s == pyfame.Status.HSUCC:
            values[name] = v

    index = pyfame.fame_range_to_pandas_index(fame_range)

    return status, pd.DataFrame(data=values, index=index)


def _batch_write(f: Callable,
                 db_key: DatabaseKey,
                 data: pd.DataFrame | pd.Series) -> Dict[ObjectName, Status]:
    if isinstance(data, pd.Series):
        data = data.to_frame()

    fame_range = pyfame.pandas_index_to_fame_range(data.index)

    return {object_name: f(db_key=db_key,
                           name=object_name,
                           fame_range=fame_range,
                           values=series.values)
            for object_name, series in data.items()}


def batch_read_booleans(db_key: DatabaseKey,
                        names: List[ObjectName] | ObjectName,
                        fame_range: FameRange
                        ) -> Tuple[Dict[ObjectName, Status], pd.DataFrame]:
    return _batch_read(f=pyfame.read_booleans, db_key=db_key, names=names, fame_range=fame_range)


def batch_write_booleans(db_key: DatabaseKey,
                         data: pd.DataFrame | pd.Series) -> Dict[ObjectName, Status]:
    return _batch_write(f=pyfame.write_booleans, db_key=db_key, data=data)


def batch_read_numerics(db_key: DatabaseKey,
                        names: List[ObjectName] | ObjectName,
                        fame_range: FameRange
                        ) -> Tuple[Dict[ObjectName, Status], pd.DataFrame]:
    return _batch_read(f=pyfame.read_numerics, db_key=db_key, names=names, fame_range=fame_range)


def batch_write_numerics(db_key: DatabaseKey,
                         data: pd.DataFrame | pd.Series) -> Dict[ObjectName, Status]:
    return _batch_write(f=pyfame.write_numerics, db_key=db_key, data=data)


def batch_read_precisions(db_key: DatabaseKey,
                          names: List[ObjectName] | ObjectName,
                          fame_range: FameRange
                          ) -> Tuple[Dict[ObjectName, Status], pd.DataFrame]:
    return _batch_read(f=pyfame.read_precisions, db_key=db_key, names=names, fame_range=fame_range)


def batch_write_precisions(db_key: DatabaseKey,
                           data: pd.DataFrame | pd.Series) -> Dict[ObjectName, Status]:
    return _batch_write(f=pyfame.write_precisions, db_key=db_key, data=data)


def batch_read_strings(db_key: DatabaseKey,
                       names: List[ObjectName] | ObjectName,
                       fame_range: FameRange
                       ) -> Tuple[Dict[ObjectName, Status], pd.DataFrame]:
    return _batch_read(f=pyfame.read_strings, db_key=db_key, names=names, fame_range=fame_range)


def batch_write_strings(db_key: DatabaseKey,
                        data: pd.DataFrame | pd.Series) -> Dict[ObjectName, Status]:
    return _batch_write(f=pyfame.write_strings, db_key=db_key, data=data)
